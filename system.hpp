#pragma once

#include "generic.hpp"
#include "basis/basisGeneric.hpp"
#include "utils.hpp"
#include "contraction.hpp"
#include "mpl.hpp"

#include <vector>
#include <type_traits>
#include <memory>

namespace jdft
{
//-----------------------------------------------------------------------------
struct System
{
  System() : numElectrons(-1) {}

  template<typename AtomType>
  mpl_enable_if<std::is_base_of<BasisAtom, AtomType>::value, void>
  addAtom(const AtomType& atom)
  {
     atoms.push_back(std::shared_ptr<BasisAtom>(new AtomType(atom)));
     for(auto basisFunPtr : atom.getBasisFuns())
        basisFuns.push_back(basisFunPtr);
  }

  void setNumElectrons(int _numElectrons)
  {
    numElectrons = _numElectrons;
  }
  int calcNumElectrons()
  {
    numElectrons = 0;
    for(auto atomPtr : atoms)
      numElectrons += atomPtr->getNumBasisFuns();
    return numElectrons;
  }

  int numElectrons;
  std::vector<std::shared_ptr<BasisAtom>> atoms;
  std::vector<Contraction> basisFuns;

  private:
    System(const System&);
    System& operator=(const System&);

};
//-----------------------------------------------------------------------------
struct NumericGrid
{
};
//-----------------------------------------------------------------------------
struct Density
{
    Density(const System& _system) : system(_system) {}

    fptype getValue(const vec3f& position)
    {
        double retVal = 0.;
        for(auto atom : system.atoms)
        {
          for(auto basisFun : atom->getBasisFuns())
          {
            retVal += basisFun(position);
          }
        }
      return retVal*retVal;
    }

    const System& system;
};
//-----------------------------------------------------------------------------
} // End of namespace jdft

#include <iostream>
//#include <chrono>
//#include <ctime>

#include "kineticIntegral.hpp"
#include "electronRepulsionIntegral.hpp"

using namespace std;
using namespace jdft;

int main()
{

float f = 0;


GTO<6,7,8> g1(1.,1.,vec3f(0.,0.,0.));
GTO<9,10,11> g2(1.,1.,vec3f(1.,2.,1.));
GTO<6,8,10> g3(6.,1.,vec3f(1.,1.,3.));
GTO<7,8,9> g4(1.,7.,vec3f(6.,1.,1.));
GTO<8,9,10> g5(4.,1.,vec3f(1.,1.,4.));
GTO<3,3,3> g6(1.,3.,vec3f(7.,1.,1.));

//auto start = std::chrono::steady_clock::now();

f += kineticEnergyIntegral(g2,g1);
f += kineticEnergyIntegral(g2,g3);
f += kineticEnergyIntegral(g2,g4);
f += kineticEnergyIntegral(g2,g5);
f += kineticEnergyIntegral(g3,g1);
f += kineticEnergyIntegral(g4,g1);
f += kineticEnergyIntegral(g5,g1);
f += kineticEnergyIntegral(g3,g4);
f += kineticEnergyIntegral(g3,g5);
f += kineticEnergyIntegral(g4,g5);
f += kineticEnergyIntegral(g6,g5);
f += kineticEnergyIntegral(g6,g4);
f += kineticEnergyIntegral(g6,g3);
f += kineticEnergyIntegral(g6,g2);
f += kineticEnergyIntegral(g6,g1);
f += kineticEnergyIntegral(g1,g1);
f += kineticEnergyIntegral(g2,g2);
f += kineticEnergyIntegral(g3,g3);
f += kineticEnergyIntegral(g4,g4);
f += kineticEnergyIntegral(g5,g5);
f += kineticEnergyIntegral(g6,g6);
f += electronRepulsionIntegral(g6,g6,g6,g6);

//auto end = std::chrono::steady_clock::now();
//auto duration = end - start;
//std::cout << chrono::duration <double, std::milli> (duration).count() << " ms" << std::endl;

cout << f << endl;
}


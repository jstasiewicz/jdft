#pragma once

#include "generic.hpp"
#include "utils.hpp"
#include "contraction.hpp"
#include "mpl.hpp"

namespace jdft
{
//-----------------------------------------------------------------------------
template <int k, int l1, int l2>
inline fptype f(fptype PA_p, fptype PB_p)
{
  fptype retVal = 0.;
  const int start = std::max(-k, k-2*l2),
            stop  = std::min( k, 2*l1-k);
  for(int q = start; q <= stop; q+=2)
  {
    const int i = (k + q)/2,
              j = (k - q)/2;
    retVal += binomial[l1][i]*binomial[l2][j]
                * ::pow(PA_p,l1-i)*::pow(PB_p,l2-j);
  }
  return retVal;
}
//-----------------------------------------------------------------------------
template <int i, int iMax>
struct EvalF
{
  template<int l1, int l2>
  inline static fptype eval(fptype PAx, fptype PBx, fptype gamma_x_2)
  {
    return f<2*i,l1,l2>(PAx, PBx) * bifactorial[2*i-1] / ::pow(gamma_x_2, i)
             + mpl_if<
                      lt<i, iMax>::value,
                      EvalF<i+1,iMax>,
                      EvalF<i-1,iMax>
                     >::type::template eval<l1,l2>(PAx, PBx, gamma_x_2);
  }
};
template <int i>
struct EvalF<i,i>
{
  template<int l1, int l2>
  static fptype eval(fptype PAx, fptype PBx, fptype gamma_x_2)
  {
    return f<2*i,l1,l2>(PAx, PBx) * bifactorial[2*i-1] / ::pow(gamma_x_2, i);
  }
};
//-----------------------------------------------------------------------------
template <int l1, int m1, int n1,
          int l2, int m2, int n2>
inline fptype overlapIntegralBase(const GTOBase& g1, const GTOBase& g2)
{
  fptype my_gamma  = g1.b + g2.b,
         gamma_x_2 = 2.*my_gamma;

  vec3f P  = (g1.b*g1.center + g2.b*g2.center)/my_gamma,
        PA = P - g1.center,
        PB = P - g2.center,
        AB = g1.center - g2.center;

  fptype Ix = 0., Iy = 0., Iz = 0.;

  Ix = EvalF<0,(l1+l2)/2>::template eval<l1,l2>(PA.x, PB.x, gamma_x_2);
  Iy = EvalF<0,(m1+m2)/2>::template eval<m1,m2>(PA.y, PB.y, gamma_x_2);
  Iz = EvalF<0,(n1+n2)/2>::template eval<n1,n2>(PA.z, PB.z, gamma_x_2);

  return g1.coef*g2.coef
           * ::exp(-g1.b*g2.b*AB.norm()/my_gamma)
           * ::pow(::sqrt(M_PI/my_gamma), 3) * (Ix*Iy*Iz);
}
//-----------------------------------------------------------------------------
template <int l1, int m1, int n1,
          int l2, int m2, int n2>
fptype overlapIntegral(const GTO<l1,m1,n1>& g1, const GTO<l2,m2,n2>& g2)
{//Convenience function
  return overlapIntegralBase<l1,m1,n1,l2,m2,n2>(g1,g2);
}
//-----------------------------------------------------------------------------

} // End of namespace jdft

#pragma once

#include <tuple>

#include <string>
#include <iostream>

#include "generic.hpp"
#include "mpl.hpp"
#include "utils.hpp"

#include "gto.hpp"

namespace jdft
{
//-----------------------------------------------------------------------------
struct Contraction
{
    Contraction(std::initializer_list<std::shared_ptr<GTOBase>> _gtoptrs)
      : gtos(_gtoptrs) {}
    
    void move(const vec3f &_vec);
    fptype operator()(const vec3f& pos) const;

    friend std::ostream& operator<<(std::ostream &os, const Contraction& c);
    
    std::vector<std::shared_ptr<GTOBase>> gtos;

  private:
};
    
std::ostream& operator<<(std::ostream &os, const Contraction& c)
{
    
    return os;
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
} // End of namespace jdft

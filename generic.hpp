#pragma once

#include <complex>
#include <cmath>

#include "Eigen/Eigen"

#if defined __APPLE__ || defined __clang__
#define JDFT_RESTRICT 
#else
#define JDFT_RESTRICT __restrict__
#endif

#define RECURRENCE_THRESHOLD 14

namespace jdft
{
//-----------------------------------------------------------------------------
typedef float fptype;
typedef std::complex<fptype> ComplexType;
//-----------------------------------------------------------------------------
typedef Eigen::Matrix<ComplexType, Eigen::Dynamic, Eigen::Dynamic> MatrixXc;
typedef Eigen::Matrix<ComplexType, Eigen::Dynamic, 1             > VectorXc;

template <size_t N>
struct VectorNc : public Eigen::Matrix<ComplexType, Eigen::Dynamic, N> {};
//-----------------------------------------------------------------------------

const fptype mp    = 1836.F,
             RANGE = 3.F,
             STEP  = .2F;
//-----------------------------------------------------------------------------
} // End of namespace jdft

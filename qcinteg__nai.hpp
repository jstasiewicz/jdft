/* qcinteg__nai.hpp
 *
 * Nuclear Attraction Integral
 */

/*
    Copyright 2008, 2009, Jan Šmydke
     All rights reserved.

    This file is a part of "qcinteg"

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

      1. Redistributions of source code must retain the above copyright
         notice, this list of conditions and the following disclaimer.
      2. Redistributions in binary form must reproduce the above copyright
         notice, this list of conditions and the following disclaimer in the
         documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstdlib> // calloc
#include <cmath>
#include "orbitals.hpp"

namespace jdft {
/*
#define hci000(n,alpha,x,y,z) (pow(-2*alpha,(n)) * qcinteg__boys((n), alpha * (x*x + y*y + z*z)))

#define key_000 0
#define key_100 256
#define key_010 16
#define key_001 1
#define key_200 512
#define key_020 32
#define key_002 2
#define key_110 272
#define key_101 257
#define key_011 17
#define key_300 768
#define key_030 48
#define key_003 3
#define key_210 528
#define key_201 513
#define key_120 288
#define key_021 33
#define key_102 258
#define key_012 18
#define key_111 273

//-----------------------------------------------------------------------------
  
const double EPSILON = 0.2;

double qcinteg__boys(int n, double x)
{
    double F;
    double ex; // auxiliary exp(-x)
    double twon; // auxiliary 2*n
    int i;

    if (x > EPSILON){ // recursion (unrolled)
  ex = exp(-x);
  F = sqrt(M_PI_4 / x) * erf(sqrt(x));
  for(i=1; i<=n; i++)
      F = ((2*i-1) * F - ex) / (2*x);

    } else { // Taylor
  // if x == 0.2; n == 0
  // => max error = R6(0.2) < 2e-10
  twon = 2 * n;
        F =  1/(twon+1) - x *
      (1/(twon+3) - (x/2) *
      (1/(twon+5) - (x/3) *
      (1/(twon+7) - (x/4) *
      (1/(twon+9) - (x/5) *
      (1/(twon+11) - (x/6)/(twon+13)
       )))));
    }

    return F;
}

//-----------------------------------------------------------------------------
double qcinteg__gg_in_h(int i,double zetaA,double a_x, int j,double zetaB, double b_x, int t)
{

    double E, zetaC, center, ab_x;

    // HGTO exponent
    zetaC = (zetaA + zetaB);

    // HGTO center
    center = (zetaA * a_x + zetaB * b_x) / zetaC;

    ab_x = b_x - a_x;

    if(t < 0 || t > (i + j))
    {
        E = 0.0;
    }
    else
    {
        if(t==0 && i==0 && j==0)
  {
            E = exp(- zetaA * zetaB * ab_x*ab_x/(zetaA + zetaB));
        }
  else
  {
            if(i > 0) // i>0, j>=0, t>=0
      {
    if(t==0)
    {
        if(i==1 && j==0)
        {
      // E = qcinteg__gg_in_h(0,zetaA,a_x, 0,zetaB,b_x, 0) * (center - a_x);
      E = exp(- zetaA * zetaB * ab_x*ab_x/(zetaA + zetaB)) * (center - a_x);
        }
        else
        {
      E = qcinteg__gg_in_h(i-1,zetaA,a_x, j,zetaB,b_x, 1) +\
          qcinteg__gg_in_h(i-1,zetaA,a_x, j,zetaB,b_x, 0) * (center - a_x);
        }
    }
    else // i>0, j>=0, t>0
    {
        if((t+2) > (i+j))
        {
      if((t+1) > (i+j))
      {
          if(t > (i+j))
          {
        E = 0.0;
          }
          else
          {
        E = qcinteg__gg_in_h(i-1,zetaA,a_x, j,zetaB,b_x, t-1) / (2*zetaC);
          }
      }
      else
      {
          E = qcinteg__gg_in_h(i-1,zetaA,a_x, j,zetaB,b_x, t) * (center - a_x) +\
        qcinteg__gg_in_h(i-1,zetaA,a_x, j,zetaB,b_x, t-1) / (2*zetaC);
      }
        }
        else
        {
      E = qcinteg__gg_in_h(i-1,zetaA,a_x, j,zetaB,b_x, t+1) * (t+1) +\
          qcinteg__gg_in_h(i-1,zetaA,a_x, j,zetaB,b_x, t) * (center - a_x) +\
          qcinteg__gg_in_h(i-1,zetaA,a_x, j,zetaB,b_x, t-1) / (2*zetaC);
        }
    }
            }
      else // i==0, j>0
      {
                if(t == 0)
    {
        if(j==1)
        {
      // E = qcinteg__gg_in_h(0,zetaA,a_x, 0,zetaB,b_x, 0) * (center - b_x);
      E = exp(- zetaA * zetaB * ab_x*ab_x/(zetaA + zetaB)) * (center - b_x);
        }
        else // i==0, j>1
        {
      E = qcinteg__gg_in_h(0,zetaA,a_x, j-1,zetaB,b_x, 1) +\
          qcinteg__gg_in_h(0,zetaA,a_x, j-1,zetaB,b_x, 0) * (center - b_x);
        }
    }
    else // i==0, j>0, t>0
    {
        if(t > j)
        {
      E = 0.0;
        }
        else // i==0, j>0, 0 < t <= j
        {
      if(t==j)
      {
          E = qcinteg__gg_in_h(0,zetaA,a_x, j-1,zetaB,b_x, t-1) / (2*zetaC);
      }
      else // i==0, j>0, 0 < t < j
      {
          E = qcinteg__gg_in_h(0,zetaA,a_x, j-1,zetaB,b_x, t+1) * (t+1) +\
        qcinteg__gg_in_h(0,zetaA,a_x, j-1,zetaB,b_x, t) * (center - b_x) +\
        qcinteg__gg_in_h(0,zetaA,a_x, j-1,zetaB,b_x, t-1) / (2*zetaC);
      }
        }
                }
            }
        }
    }

    return E;
}

//-----------------------------------------------------------------------------

double qcinteg__hci(int n, int t,int u,int v, double alpha, double pc_x, double pc_y, double pc_z)
{
    double result;

    unsigned int key = 0;


    
    if(t>14)
    {
  key |= 15; // ie at most 14 will be treated individually
    }
    else
    {
  key |= (unsigned int) t;
    }
    key <<= 4;
    if(u>14)
    {
  key |= 15;
    }
    else
    {
  key |= (unsigned int) u;
    }
    key <<= 4;
    if(v>14)
    {
  key |= 15;
    }
    else
    {
  key |= (unsigned int) v;
    }

switch (key)
    {
  case key_000:
      result = hci000(n, alpha, pc_x, pc_y, pc_z);
      break;
  case key_100:
      result = pc_x * hci000(n+1, alpha, pc_x, pc_y, pc_z);
      break;
  case key_010:
      result = pc_y * hci000(n+1, alpha, pc_x, pc_y, pc_z);
      break;
  case key_001:
      result = pc_z * hci000(n+1, alpha, pc_x, pc_y, pc_z);
      break;
  case key_200:
      result = hci000(n+1, alpha, pc_x, pc_y, pc_z) + pc_x*pc_x*hci000(n+2, alpha, pc_x, pc_y, pc_z);
      break;
  case key_020:
      result = hci000(n+1, alpha, pc_x, pc_y, pc_z) + pc_y*pc_y*hci000(n+2, alpha, pc_x, pc_y, pc_z);
      break;
  case key_002:
      result = hci000(n+1, alpha, pc_x, pc_y, pc_z) + pc_z*pc_z*hci000(n+2, alpha, pc_x, pc_y, pc_z);
      break;
  case key_110:
      result = pc_x * pc_y * hci000(n+2, alpha, pc_x, pc_y, pc_z);
      break;
  case key_101:
      result = pc_x * pc_z * hci000(n+2, alpha, pc_x, pc_y, pc_z);
      break;
  case key_011:
      result = pc_y * pc_z * hci000(n+2, alpha, pc_x, pc_y, pc_z);
      break;
  case key_300:
      result = 3*pc_x*hci000(n+2, alpha, pc_x, pc_y, pc_z) + pc_x*pc_x*pc_x*hci000(n+3, alpha, pc_x, pc_y, pc_z);
      break;
  case key_030:
      result = 3*pc_y*hci000(n+2, alpha, pc_x, pc_y, pc_z) + pc_y*pc_y*pc_y*hci000(n+3, alpha, pc_x, pc_y, pc_z);
      break;
  case key_003:
      result = 3*pc_z*hci000(n+2, alpha, pc_x, pc_y, pc_z) + pc_z*pc_z*pc_z*hci000(n+3, alpha, pc_x, pc_y, pc_z);
      break;
  case key_210:
      result = pc_y*hci000(n+2, alpha, pc_x, pc_y, pc_z) + pc_x*pc_y*hci000(n+3, alpha, pc_x, pc_y, pc_z);
      break;
  case key_201:
      result = pc_z*hci000(n+2, alpha, pc_x, pc_y, pc_z) + pc_x*pc_z*hci000(n+3, alpha, pc_x, pc_y, pc_z);
      break;
  case key_120:
      result = pc_x*hci000(n+2, alpha, pc_x, pc_y, pc_z) + pc_x*pc_y*hci000(n+3, alpha, pc_x, pc_y, pc_z);
      break;
  case key_021:
      result = pc_z*hci000(n+2, alpha, pc_x, pc_y, pc_z) + pc_y*pc_z*hci000(n+3, alpha, pc_x, pc_y, pc_z);
      break;
  case key_102:
      result = pc_x*hci000(n+2, alpha, pc_x, pc_y, pc_z) + pc_x*pc_z*hci000(n+3, alpha, pc_x, pc_y, pc_z);
      break;
  case key_012:
      result = pc_y*hci000(n+2, alpha, pc_x, pc_y, pc_z) + pc_y*pc_z*hci000(n+3, alpha, pc_x, pc_y, pc_z);
      break;
  case key_111:
      result = pc_x * pc_y * pc_z * hci000(n+3, alpha, pc_x, pc_y, pc_z);
  default:
      if(t > 0)
      {
    if(t==1)
    {
        result = pc_x * qcinteg__hci(n+1, 0,u,v, alpha, pc_x, pc_y, pc_z);
    }
    else // t>1
    {
        result = (t-1)*qcinteg__hci(n+1, t-2,u,v, alpha, pc_x, pc_y, pc_z) +\
      pc_x*qcinteg__hci(n+1, t-1,u,v, alpha, pc_x, pc_y, pc_z);
    }
      }
      else // t==0
      {
    if(u > 0)
    {
        if(u==1)
        {
      result = pc_y * qcinteg__hci(n+1, 0,0,v, alpha, pc_x, pc_y, pc_z);
        }
        else
        {
      result = (u-1)*qcinteg__hci(n+1, 0,u-2,v, alpha, pc_x, pc_y, pc_z) +\
          pc_y*qcinteg__hci(n+1, 0,u-1,v, alpha, pc_x, pc_y, pc_z);
        }
    }
    else // t==0, u==0, v>3
    {
        result = (v-1)*qcinteg__hci(n+1, 0,0,v-2, alpha, pc_x, pc_y, pc_z) +\
      pc_z*qcinteg__hci(n+1, 0,0,v-1, alpha, pc_x, pc_y, pc_z);
    }
      }
      break;
    }

    return result;
}
*/
//-----------------------------------------------------------------------------

/**
 Nuclear Attraction Integral of two 3Dim cartesian primitive GTOs

 GTOs centered on A, B
 Nucleus centered on N

 For one electron and a nucleus with charge Z,
 don't forget to multiply by Z
 */
template<int iA, int jA, int kA,
         int iB, int jB, int kB>
double qcinteg__nai_base(const GTOBase& A,  const GTOBase& B,  const vec3f& N)
{
    double zetaA = A.b, zetaB = B.b;
    double xyzA[3] = {A.center.x, A.center.y, A.center.z},
           xyzB[3] = {B.center.x, B.center.y, B.center.z},
           xyzN[3] = {N.x, N.y, N.z};
  
    double result;
    double zetaP;

    double p_x, p_y, p_z;
    double pn_x, pn_y, pn_z;

    double e_til[iA + jA + kA + iB + jB + kB + 3];
    double *e_ujm, *e_vkn;

    int t, u, v;

    e_ujm = e_til + iA + iB + 1;// ijkA[0]+ijkB[0] + 1;
    e_vkn = e_ujm + jA + jB + 1;// ijkA[1]+ijkB[1] + 1;

    // HGTO exponent
    zetaP = zetaA + zetaB;

    // center P
    p_x = (zetaA*xyzA[0] + zetaB*xyzB[0]) / zetaP;
    p_y = (zetaA*xyzA[1] + zetaB*xyzB[1]) / zetaP;
    p_z = (zetaA*xyzA[2] + zetaB*xyzB[2]) / zetaP;
    
    
    
    pn_x = (p_x - xyzN[0]); // !!! here Rab = a - b
    pn_y = (p_y - xyzN[1]); // !!!
    pn_z = (p_z - xyzN[2]); // !!!

    for(t=0; t <= (iA+iB); t++)
        e_til[t] = qcinteg__gg_in_h(iA,zetaA,xyzA[0], iB,zetaB,xyzB[0], t);

    for(u=0; u <= (jA+jB); u++)
        e_ujm[u] = qcinteg__gg_in_h(jA,zetaA,xyzA[1], jB,zetaB,xyzB[1], u);

    for(v=0; v <= (kA+kB); v++)
        e_vkn[v] = qcinteg__gg_in_h(kA,zetaA,xyzA[2], kB,zetaB,xyzB[2], v);


    // Summation
    result = 0.;

    for(t=0; t <= (iA+iB); t++){
        for(u=0; u <= (jA+jB); u++){
            for(v=0; v <= (kA+kB); v++){

                result += e_til[t]*e_ujm[u]*e_vkn[v] * qcinteg__hci(0, t,u,v, zetaP, pn_x,pn_y,pn_z);

            }
        }
    }

    result *= -(2 * M_PI / zetaP);

    return result;
}
//-----------------------------------------------------------------------------
template <int l1, int m1, int n1,
          int l2, int m2, int n2>
fptype qcinteg__nai(const GTO<l1, m1, n1>& g1, 
                    const GTO<l2, m2, n2>& g2, 
                    const vec3f& N)
{
  return qcinteg__nai_base<l1,m1,n1,l2,m2,n2>(g1, g2, N);
}
//-----------------------------------------------------------------------------

} // end of namespace jdft

#pragma once

#include <tuple>
#include <iostream>

#include "generic.hpp"
#include "utils.hpp"
#include "contraction.hpp"
#include "mpl.hpp"

#include "integrals.hpp"

namespace jdft
{
//-----------------------------------------------------------------------------
void testOI()
{
  std::cout << "testOI():   ";
  fptype results = 0.;

  results += fabs(overlapIntegral(G000(3, 2,vec3f(1,0,0)), G001(2, 1,vec3f(1,1,1))) + 0.0542181)/0.0542181;
  results += fabs(overlapIntegral(G310(2, 2,vec3f(0,1,0)), G110(3, 2,vec3f(1,1,1))) + 0.000368683)/0.000368683;
  results += fabs(overlapIntegral(G030(3, 3,vec3f(0,0,1)), G100(4, 7,vec3f(1,1,1))) + 0.027122)/0.027122;
  results += fabs(overlapIntegral(GTO<3,2,1>(3,100,vec3f(0,1,2)), GTO<3,4,5>(2,200,vec3f(4,5,6))) - 5.36668e-16)/5.36668e-16;
  results += fabs(overlapIntegral(GTO<3,2,1>(3.5,1,vec3f(0,1,2)), GTO<3,4,5>(2.5,1,vec3f(2,3,4))) - 1.48119e-8)/1.48119e-8;

  if(results > 1.e-4) std::cout << "FAILED, error = " << results << std::endl;
  else std::cout << "OK" << std::endl;
}
//-----------------------------------------------------------------------------
void testKEI()
{
  std::cout << "testKEI():  ";
  fptype results = 0.;

  results += fabs(kineticEnergyIntegral(GTO<3,2,1>(3.5,1,vec3f(0,1,2)), GTO<3,4,5>(2.5,1,vec3f(2,3,4))) + 6.01229e-8)/6.01229e-8;
  results += fabs(kineticEnergyIntegral(GTO<0,0,1>(1.,1.,vec3f(0,0,0)), GTO<0,0,1>(1.,1.,vec3f(0,0,0.1))) - 1.20474)/1.20474;
  results += fabs(kineticEnergyIntegral(GTO<1,2,0>(3.5,1,vec3f(1,1,2)), GTO<1,0,4>(2.5,1,vec3f(2,3,2))) - 8.49561e-6)/8.49561e-6;
  results += fabs(kineticEnergyIntegral(GTO<1,2,0>(3.5,2,vec3f(1,1,2)), GTO<1,0,4>(2.5,3,vec3f(2,3,2))) - 0.0000509737)/0.0000509737;
  results += fabs(kineticEnergyIntegral(GTO<1,2,0>(5,2,vec3f(1,1,2)),   GTO<1,3,4>(3,3,vec3f(2,3,2))) + 3.82305e-6)/3.82305e-6;

  if(results > 2.e-3) std::cout << "FAILED, error = " << results << std::endl;
  else std::cout << "OK" << std::endl;
}
//-----------------------------------------------------------------------------
void testNAI()
{
  std::cout << "testNAI():  ";
  fptype results = 0.;

  results += fabs(nuclearAttractionIntegral(GTO<3,2,1>(3.5,1,vec3f(0,1,2)), GTO<3,4,5>(2.5,1,vec3f(2,3,4)), vec3f(2,1,3)) + 1.06222e-8)/1.06222e-8;
  results += fabs(nuclearAttractionIntegral(GTO<0,0,1>(1.,1.,vec3f(0,0,0)), GTO<0,0,1>(1.,1.,vec3f(0,0,0.1)), vec3f(2,1,3)) + 0.133159)/0.133159;
  results += fabs(nuclearAttractionIntegral(GTO<1,2,0>(3.5,1,vec3f(1,1,2)), GTO<1,0,4>(2.5,1,vec3f(2,3,2)), vec3f(2,1,3)) - 4.54706e-7)/4.54706e-7;
  results += fabs(nuclearAttractionIntegral(GTO<1,2,0>(3.5,2,vec3f(1,1,2)), GTO<1,0,4>(2.5,3,vec3f(2,3,2)), vec3f(2,1,3)) - 27.2824e-7)/27.2824e-7;
  results += fabs(nuclearAttractionIntegral(GTO<1,2,0>(5,2,vec3f(1,1,2)),   GTO<1,3,4>(3,3,vec3f(2,3,2)), vec3f(2,1,3)) + 1.79335e-7)/1.79335e-7;

  if(results > 0.2) std::cout << "FAILED, error = " << results << std::endl;
  else std::cout << "OK" << std::endl;
}
//-----------------------------------------------------------------------------
void testERI()
{
  std::cout << "testERI():  ";
  fptype results = 0.;

  results += electronRepulsionIntegral(GTO<3,2,1>(3.5,1,vec3f(0,1,2)), GTO<3,4,5>(2.5,1,vec3f(2,3,4)),
                                       GTO<3,2,1>(3.5,1,vec3f(0,1,2)), GTO<3,4,5>(2.5,1,vec3f(2,3,4)) );
  std::cout << results << std::endl;

//   results += fabs(electronRepulsionIntegral(GTO<0,0,1>(1.,1.,vec3f(0,0,0)), GTO<0,0,1>(1.,1.,vec3f(0,0,0.1)), vec3f(2,1,3)) + 0.133159)/0.133159;
//   results += fabs(electronRepulsionIntegral(GTO<1,2,0>(3.5,1,vec3f(1,1,2)), GTO<1,0,4>(2.5,1,vec3f(2,3,2)), vec3f(2,1,3)) - 4.54706e-7)/4.54706e-7;
//   results += fabs(electronRepulsionIntegral(GTO<1,2,0>(3.5,2,vec3f(1,1,2)), GTO<1,0,4>(2.5,3,vec3f(2,3,2)), vec3f(2,1,3)) - 27.2824e-7)/27.2824e-7;
//   results += fabs(electronRepulsionIntegral(GTO<1,2,0>(5,2,vec3f(1,1,2)),   GTO<1,3,4>(3,3,vec3f(2,3,2)), vec3f(2,1,3)) + 1.79335e-7)/1.79335e-7;

  if(results > 1e-4) std::cout << "FAILED, error = " << results << std::endl;
  else std::cout << "OK" << std::endl;
}
//-----------------------------------------------------------------------------
void testAll()
{
  std::cout << "Testing all..." << std::endl;

  testOI();
  testKEI();
  testNAI();
  testERI();
}
//-----------------------------------------------------------------------------
} // End of namespace jdft

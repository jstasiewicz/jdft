#pragma once

#include <string>
#include <iostream>

#include "generic.hpp"
#include "utils.hpp"

#include "gto.hpp"

namespace jdft
{
//-----------------------------------------------------------------------------
/** Internal implementation of RTTI. Some kinds of GTOs are
    pruned to avoid code bloat. Feel free to extend it if you encounter
    GTOs of higher angular momenta. */
    enum class GTOTypesEnum : int
{
    G000,
    G100, G010, G001,
    G200, G020, G002, G110, G011, G101,
    G300, G030, G003, G210, G120, G012, G201, G021, G102, G111,
    G400, G040, G004, G310, G130, G103, G301, G031, G013, G220, G022, G202, G211, G121, G112
};
//-----------------------------------------------------------------------------
/** Struct such that GTOTypeToCode<G000>::valueCode
    is the code (in terms of GTOTypesEnum) of given GTO; */
    
template<GTOType>
struct GTOTypeToCode {};

template<> struct GTOTypeToCode<G000>
{ static const GTOTypesEnum valueCode = GTOTypesEnum::G000; };
template<> struct GTOTypeToCode<G100>
{ static const GTOTypesEnum valueCode = GTOTypesEnum::G100; };
template<> struct GTOTypeToCode<G010>
{ static const GTOTypesEnum valueCode = GTOTypesEnum::G010; };
template<> struct GTOTypeToCode<G001>
{ static const GTOTypesEnum valueCode = GTOTypesEnum::G001; };
template<> struct GTOTypeToCode<G110>
{ static const GTOTypesEnum valueCode = GTOTypesEnum::G110; };
template<> struct GTOTypeToCode<G011>
{ static const GTOTypesEnum valueCode = GTOTypesEnum::G011; };
template<> struct GTOTypeToCode<G101>
{ static const GTOTypesEnum valueCode = GTOTypesEnum::G101; };
template<> struct GTOTypeToCode<G200>
{ static const GTOTypesEnum valueCode = GTOTypesEnum::G200; };
template<> struct GTOTypeToCode<G020>
{ static const GTOTypesEnum valueCode = GTOTypesEnum::G020; };
template<> struct GTOTypeToCode<G002>
{ static const GTOTypesEnum valueCode = GTOTypesEnum::G002; };
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
template <GTOTypesEnum>
struct GTOCodeToType {}
    
template<> struct GTOCodeToType<GTOTypesEnum::G000>
{ typedef G000 type;}
template<> struct GTOCodeToType<GTOTypesEnum::G100>
{ typedef G100 type;}
template<> struct GTOCodeToType<GTOTypesEnum::G010>
{ typedef G010 type;}
template<> struct GTOCodeToType<GTOTypesEnum::G001>
{ typedef G001 type;}
template<> struct GTOCodeToType<GTOTypesEnum::G110>
{ typedef G110 type;}
template<> struct GTOCodeToType<GTOTypesEnum::G011>
{ typedef G011 type;}
template<> struct GTOCodeToType<GTOTypesEnum::G101>
{ typedef G101 type;}
template<> struct GTOCodeToType<GTOTypesEnum::G200>
{ typedef G200 type;}
template<> struct GTOCodeToType<GTOTypesEnum::G020>
{ typedef G020 type;}
template<> struct GTOCodeToType<GTOTypesEnum::G002>
{ typedef G002 type;}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
} // End of namespace jdft

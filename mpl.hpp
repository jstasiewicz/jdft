#pragma once

namespace jdft
{
//-----------------------------------------------------------------------------
template <bool value, typename T, typename U>
struct mpl_if
{
  typedef U type;
};
template <typename T, typename U>
struct mpl_if<true, T, U>
{
  typedef T type;
};
//-----------------------------------------------------------------------------
template <int n, int k>
struct lt
{
  static const bool value = (n < k);
};
template <int n, int k>
struct gt
{
  static const bool value = (n > k);
};
template <int n, int k>
struct leq
{
  static const bool value = (n <= k);
};
template <int n, int k>
struct geq
{
  static const bool value = (n >= k);
};
//-----------------------------------------------------------------------------
template<bool value, typename T>
struct mpl_enable_if
{
};
template<typename T>
struct mpl_enable_if<true, T>
{
  typedef T type;
};
//-----------------------------------------------------------------------------
template<bool value>
struct mpl_enable_fptype_if
{
};
template<>
struct mpl_enable_fptype_if<true>
{
  typedef fptype type;
};
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
#pragma inline_recursion(on)
//-----------------------------------------------------------------------------
/** For-loop in template metalanguage.
 * \code
   mpl_for<5, mpl_for<5>>::eval(functor); // Usage.
   //The equivalent (noncompilable; just for illustration):
   for(int i = 0; i <= 5; ++i)
     for(int j = 0; j <= 7; ++j)
       functor.template eval<i,j>();
 * \endcode
 * The functor must implement method void eval(), templated with some integers.
 *  Code written by Jesus himself with the modest hands of JS.*/
template<typename What, int iteration>
struct mpl_for_impl
{
  template<int... indices, typename FunctorType>
  inline static void eval(FunctorType& functor)
  {
    What::template eval<indices..., iteration>(functor);
    mpl_for_impl<What, iteration-1>::template eval<indices...>(functor);
  }
};
template<typename What>
struct mpl_for_impl<What, 0>
{
  template<int... indices, typename FunctorType>
  inline static void eval(FunctorType& functor)
  {
    What::template eval<indices..., 0>(functor);
  }
};
template<int iteration>
struct mpl_for_impl<void, iteration>
{
  template<int... indices, typename FunctorType>
  inline static void eval(FunctorType& functor)
  {
    functor.template eval<indices..., iteration>();
    mpl_for_impl<void, iteration-1>::template eval<indices...>(functor);
  }
};
template<>
struct mpl_for_impl<void, 0>
{
  template<int... indices, typename FunctorType>
  inline static void eval(FunctorType& functor)
  {
    functor.template eval<indices..., 0>();
  }
};

template<int numIter, typename What = void>
struct mpl_for
{
  template<int... indices, typename FunctorType>
  inline static void eval(FunctorType& functor)
  {
    mpl_for_impl<What, numIter>::template eval<indices...>(functor);
  }
};
//-----------------------------------------------------------------------------
#pragma inline_recursion(off)
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
} // End of namespace jdft

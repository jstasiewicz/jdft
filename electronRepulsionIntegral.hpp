#pragma once

#include <cmath>

#include "generic.hpp"
#include "contraction.hpp"
#include "hermiteIntegrals.hpp"

namespace jdft
{
//-----------------------------------------------------------------------------
//////////////////////////////COMPUTATIONAL LAYER//////////////////////////////
//-----------------------------------------------------------------------------
/**
 * Electron Repulsion Integral
 * of four 3Dim primitive GTOs
 */
template <int l1, int m1, int n1,
          int l2, int m2, int n2,
          int l3, int m3, int n3,
          int l4, int m4, int n4>
fptype electronRepulsionIntegralBase(const GTOBase& g1, const GTOBase& g2, const GTOBase& g3, const GTOBase& g4)
{
    fptype bP1   = g1.b + g2.b,
           bP2   = g3.b + g4.b,
           alpha = bP1*bP2 / (bP1+bP2);

    vec3f p1     = (g1.b*g1.center + g2.b*g2.center) / bP1,
          p2     = (g3.b*g3.center + g4.b*g4.center) / bP2,
          p1p2   = p1 - p2;

    JDFT_RESTRICT fptype E_til[l1 + l2 + 1],
                         E_ujm[m1 + m2 + 1],
                         E_vkn[n1 + n2 + 1],
                         E_wor[l3 + l4 + 1],
                         E_xps[m3 + m4 + 1],
                         E_yqt[n3 + n4 + 1];

    EvalHermiteExpansionFunctor<l1,l2> ehef_til(g1.b,g1.center.x, g2.b,g2.center.x, E_til);
    EvalHermiteExpansionFunctor<m1,m2> ehef_ujm(g1.b,g1.center.y, g2.b,g2.center.y, E_ujm);
    EvalHermiteExpansionFunctor<n1,n2> ehef_vkn(g1.b,g1.center.z, g2.b,g2.center.z, E_vkn);
    EvalHermiteExpansionFunctor<l3,l4> ehef_wor(g3.b,g3.center.x, g4.b,g4.center.x, E_wor);
    EvalHermiteExpansionFunctor<m3,m4> ehef_xps(g3.b,g3.center.y, g4.b,g4.center.y, E_xps);
    EvalHermiteExpansionFunctor<n3,n4> ehef_yqt(g3.b,g3.center.z, g4.b,g4.center.z, E_yqt);

    mpl_for<l1+l2>::eval(ehef_til);
    mpl_for<m1+m2>::eval(ehef_ujm);
    mpl_for<n1+n2>::eval(ehef_vkn);
    mpl_for<l3+l4>::eval(ehef_wor);
    mpl_for<m3+m4>::eval(ehef_xps);
    mpl_for<n3+n4>::eval(ehef_yqt);

    typedef HCI_Values <l1+l2+l3+l4+1, m1+m2+m3+m4+1, n1+n2+n3+n4+1> HCIValType;
    HCIValType hcival;
    EvalHCIFunctor<HCIValType> ehcif(hcival, alpha, p1p2);
    mpl_for<l1+l2+l3+l4, mpl_for<m1+m2+m3+m4, mpl_for<n1+n2+n3+n4>>>::eval(ehcif);

    fptype retVal = 0.;
    for(int t = 0; t <= l1+l2; ++t)
    {
      fptype currentVal_t = 0.;
      for(int u = 0; u <= m1+m2; ++u)
      {
        fptype currentVal_u = 0.;
        for(int v = 0; v <= n1+n2; ++v)
        {
          fptype currentVal_v = 0.;
          for(int w = 0; w <= l3+l4; ++w)
          {
            fptype currentVal_w = 0.;
            for(int x = 0; x <= m3+m4; ++x)
            {
              fptype currentVal_x = 0.;
              for(int y = 0; y <= n3+n4; ++y)
              {
                currentVal_x += ((w+x+y)%2 ? -1. : 1.) * E_yqt[y]*hcival(t+w, u+x, v+y);
              }
              currentVal_w += E_xps[x]*currentVal_x;
            }
            currentVal_v += E_wor[w]*currentVal_w;
          }
          currentVal_u += E_vkn[v]*currentVal_v;
        }
        currentVal_t += E_ujm[u]*currentVal_u;
      }
      retVal += E_til[t]*currentVal_t;
    }

    return g1.coef*g2.coef*g3.coef*g4.coef
             *retVal * 2.*::pow(M_PI, 2.5) / (bP1*bP2 * ::sqrt(bP1+bP2));
}
//-----------------------------------------------------------------------------
fptype electronRepulsionIntegralBase(int l1, int m1, int n1, int l2, int m2, int n2,
                                     int l3, int m3, int n3, int l4, int m4, int n4,
                                     const GTOBase& g1, const GTOBase& g2, const GTOBase& g3, const GTOBase& g4)
{
    fptype zetaA = g1.b, zetaB = g2.b, zetaC = g3.b, zetaD = g4.b;

    fptype zetaP1, zetaP2;
    fptype alpha;

    fptype p1_x, p1_y, p1_z, p2_x, p2_y, p2_z, p1p2_x, p1p2_y, p1p2_z;

    fptype part1;

    int tt, uu, vv, tau, nu, phi;

    JDFT_RESTRICT fptype E_til[l1 + l2 + 1],
                         E_ujm[m1 + m2 + 1],
                         E_vkn[n1 + n2 + 1],
                         E_wor[l3 + l4 + 1],
                         E_xps[m3 + m4 + 1],
                         E_yqt[n3 + n4 + 1];

    // HGTO exponents
    zetaP1 = zetaA + zetaB;
    zetaP2 = zetaC + zetaD;
    alpha = zetaP1 * zetaP2 / (zetaP1 + zetaP2);

    // HGTO centers
    p1_x = (zetaA*g1.center.x + zetaB*g2.center.x) / zetaP1;
    p1_y = (zetaA*g1.center.y + zetaB*g2.center.y) / zetaP1;
    p1_z = (zetaA*g1.center.z + zetaB*g2.center.z) / zetaP1;
    p2_x = (zetaC*g3.center.x + zetaD*g4.center.x) / zetaP2;
    p2_y = (zetaC*g3.center.y + zetaD*g4.center.y) / zetaP2;
    p2_z = (zetaC*g3.center.z + zetaD*g4.center.z) / zetaP2;
    p1p2_x = (p1_x - p2_x); // !!! here Rab = a - b
    p1p2_y = (p1_y - p2_y); // !!! and not the usual
    p1p2_z = (p1_z - p2_z); // !!! Rab = b - a

    for(tt=0; tt <= (l1+l2); tt++)
        E_til[tt] = qcinteg__gg_in_h(l1,zetaA,g1.center.x, l2,zetaB,g2.center.x, tt);

    for(uu=0; uu <= (m1+m2); uu++)
        E_ujm[uu] = qcinteg__gg_in_h(m1,zetaA,g1.center.y, m2,zetaB,g2.center.y, uu);

    for(vv=0; vv <= (n1+n2); vv++)
        E_vkn[vv] = qcinteg__gg_in_h(n1,zetaA,g1.center.z, n2,zetaB,g2.center.z, vv);

    for(tau=0; tau <= (l3+l4); tau++)
        E_wor[tau] = qcinteg__gg_in_h(l3,zetaC,g3.center.x, l4,zetaD,g4.center.x, tau);

    for(nu=0; nu <= (m3+m4); nu++)
        E_xps[nu] = qcinteg__gg_in_h(m3,zetaC,g3.center.y, m4,zetaD,g4.center.y, nu);

    for(phi=0; phi <= (n3+n4); phi++)
        E_yqt[phi] = qcinteg__gg_in_h(n3,zetaC,g3.center.z, n4,zetaD,g4.center.z, phi);



    // Summation
    fptype retVal = 0.;

    part1 = 2*pow(M_PI, 2.5) / (zetaP1*zetaP2 * sqrt(zetaP1+zetaP2));
    HCI_Values_Dynamic hciValDyn(l1+l2+l3+l4+1, m1+m2+m3+m4+1, n1+n2+n3+n4+1);

    for(int tw = 0; tw <= l1+l2+l3+l4; ++tw)
      for(int ux = 0; ux <= m1+m2+m3+m4; ++ux)
        for(int vy = 0; vy <= n1+n2+n3+n4; ++vy)
        {
          hciValDyn(tw,ux,vy) = qcinteg__hci(0, tw, ux, vy, alpha, p1p2_x,p1p2_y,p1p2_z);
        }

    for(int t=0; t <= (l1+l2); t++)
    {
      fptype currentVal_t = 0.;
      for(int u=0; u <= (m1+m2); u++)
      {
        fptype currentVal_u = 0.;
        for(int v=0; v <= (n1+n2); v++)
        {
          fptype currentVal_v = 0.;
          for(int w=0; w <= (l3+l4); w++)
          {
            fptype currentVal_w = 0.;
            for(int x=0; x <= (m3+m4); x++)
            {
              fptype currentVal_x = 0.;
              for(int y=0; y <= (n3+n4); y++)
              {
                /*
                currentVal_x += ((((w+x+y) % 2)==0) ? 1.0 : -1.0) *\
                    E_yqt[y] *\
                    qcinteg__hci(0, t+w, u+x, v+y, alpha, p1p2_x,p1p2_y,p1p2_z);
                    */
                currentVal_x += ((w+x+y) % 2 ? -1. : 1.)
                                  * E_yqt[y] * hciValDyn(t+w, u+x, v+y);

              }
              currentVal_w += E_xps[x]*currentVal_x;
            }
            currentVal_v += E_wor[w]*currentVal_w;
          }
          currentVal_u += E_vkn[v]*currentVal_v;
        }
        currentVal_t += E_ujm[u]*currentVal_u;
      }
      retVal += E_til[t]*currentVal_t;
    }

    retVal *= part1;

    return g1.coef*g2.coef*g3.coef*g4.coef*retVal;
}
//-----------------------------------------------------------------------------
////////////////////////////CUTOFF FOR DEEP RECURRENCES////////////////////////
//-----------------------------------------------------------------------------
template <int l1, int m1, int n1, int l2, int m2, int n2,
          int l3, int m3, int n3, int l4, int m4, int n4>
typename mpl_enable_fptype_if<
                             (l1+l2+l3+l4 <= RECURRENCE_THRESHOLD) &&
                             (m1+m2+m3+m4 <= RECURRENCE_THRESHOLD) &&
                             (n1+n2+n3+n4 <= RECURRENCE_THRESHOLD)
                             >::type
electronRepulsionIntegralImpl(const GTO<l1, m1, n1>& g1,
                              const GTO<l2, m2, n2>& g2,
                              const GTO<l3, m3, n3>& g3,
                              const GTO<l4, m4, n4>& g4)
{
  std::cout << "compiletime dispatch" << std::endl;
  return electronRepulsionIntegralBase<l1, m1, n1, l2, m2, n2,
                                       l3, m3, n3, l4, m4, n4>(g1, g2, g3, g4);
}
//-----------------------------------------------------------------------------
template <int l1, int m1, int n1, int l2, int m2, int n2,
          int l3, int m3, int n3, int l4, int m4, int n4>
typename mpl_enable_fptype_if<
                             (l1+l2+l3+l4 > RECURRENCE_THRESHOLD) ||
                             (m1+m2+m3+m4 > RECURRENCE_THRESHOLD) ||
                             (n1+n2+n3+n4 > RECURRENCE_THRESHOLD)
                             >::type
electronRepulsionIntegralImpl(const GTO<l1, m1, n1>& g1,
                              const GTO<l2, m2, n2>& g2,
                              const GTO<l3, m3, n3>& g3,
                              const GTO<l4, m4, n4>& g4)
{
  std::cout << "runtime dispatch" << std::endl;
  return electronRepulsionIntegralBase(l1, m1, n1, l2, m2, n2,
                                       l3, m3, n3, l4, m4, n4, g1, g2, g3, g4);
}
//-----------------------------------------------------------------------------
///////////////////////////////SHUFFLING OF INDICES////////////////////////////
//-----------------------------------------------------------------------------
// [ij|kl] == [kl|ij]
template <int l1, int m1, int n1, int l2, int m2, int n2,
          int l3, int m3, int n3, int l4, int m4, int n4>
typename mpl_enable_fptype_if<
                              (l1+m1+n1+l2+m2+n2 < l3+m3+n3+l4+m4+n4)
                             >::type
electronRepulsionIntegral3(const GTO<l1, m1, n1>& g1,
                           const GTO<l2, m2, n2>& g2,
                           const GTO<l3, m3, n3>& g3,
                           const GTO<l4, m4, n4>& g4)
{
  return electronRepulsionIntegralImpl<l3, m3, n3, l4, m4, n4,
                                       l1, m1, n1, l2, m2, n2>(g3, g4, g1, g2);
}
// No shuffling
template <int l1, int m1, int n1, int l2, int m2, int n2,
          int l3, int m3, int n3, int l4, int m4, int n4>
typename mpl_enable_fptype_if<
                              (l1+m1+n1+l2+m2+n2 >= l3+m3+n3+l4+m4+n4)
                             >::type
electronRepulsionIntegral3(const GTO<l1, m1, n1>& g1,
                           const GTO<l2, m2, n2>& g2,
                           const GTO<l3, m3, n3>& g3,
                           const GTO<l4, m4, n4>& g4)
{
  return electronRepulsionIntegralImpl<l1, m1, n1, l2, m2, n2,
                                       l3, m3, n3, l4, m4, n4>(g1, g2, g3, g4);
}
//-----------------------------------------------------------------------------
// [ij|kl] == [ji|kl]
template <int l1, int m1, int n1, int l2, int m2, int n2,
          int l3, int m3, int n3, int l4, int m4, int n4>
typename mpl_enable_fptype_if<
                              (l1+m1+n1 < l2+m2+n2)
                             >::type
    electronRepulsionIntegral2(const GTO<l1, m1, n1>& g1,
                               const GTO<l2, m2, n2>& g2,
                               const GTO<l3, m3, n3>& g3,
                               const GTO<l4, m4, n4>& g4)
{
  return electronRepulsionIntegral3<l2, m2, n2, l1, m1, n1,
                                    l3, m3, n3, l4, m4, n4>(g2, g1, g3, g4);
}
// No shuffling
template <int l1, int m1, int n1, int l2, int m2, int n2,
          int l3, int m3, int n3, int l4, int m4, int n4>
typename mpl_enable_fptype_if<
                              (l1+m1+n1 >= l2+m2+n2)
                             >::type
electronRepulsionIntegral2(const GTO<l1, m1, n1>& g1,
                           const GTO<l2, m2, n2>& g2,
                           const GTO<l3, m3, n3>& g3,
                           const GTO<l4, m4, n4>& g4)
{
  return electronRepulsionIntegral3<l1, m1, n1, l2, m2, n2,
                                    l3, m3, n3, l4, m4, n4>(g1, g2, g3, g4);
}
//-----------------------------------------------------------------------------
// [ij|kl] == [ij|lk]
template <int l1, int m1, int n1, int l2, int m2, int n2,
          int l3, int m3, int n3, int l4, int m4, int n4>
typename mpl_enable_fptype_if<
                              (l3+m3+n3 < l4+m4+n4)
                             >::type
electronRepulsionIntegral1(const GTO<l1, m1, n1>& g1,
                           const GTO<l2, m2, n2>& g2,
                           const GTO<l3, m3, n3>& g3,
                           const GTO<l4, m4, n4>& g4)
{
  return electronRepulsionIntegral2<l1, m1, n1, l2, m2, n2,
                                    l4, m4, n4, l3, m3, n3>(g1, g2, g4, g3);
}
// No shuffling
template <int l1, int m1, int n1, int l2, int m2, int n2,
          int l3, int m3, int n3, int l4, int m4, int n4>
typename mpl_enable_fptype_if<
                              (l3+m3+n3 >= l4+m4+n4)
                             >::type
electronRepulsionIntegral1(const GTO<l1, m1, n1>& g1,
                           const GTO<l2, m2, n2>& g2,
                           const GTO<l3, m3, n3>& g3,
                           const GTO<l4, m4, n4>& g4)
{
  return electronRepulsionIntegral2<l1, m1, n1, l2, m2, n2,
                                    l3, m3, n3, l4, m4, n4>(g1, g2, g3, g4);
}
//-----------------------------------------------------------------------------
//////////////////////////////////TOPLEVEL/////////////////////////////////////
//-----------------------------------------------------------------------------
template <int l1, int m1, int n1, int l2, int m2, int n2,
          int l3, int m3, int n3, int l4, int m4, int n4>
fptype
electronRepulsionIntegral(const GTO<l1, m1, n1>& g1,
                          const GTO<l2, m2, n2>& g2,
                          const GTO<l3, m3, n3>& g3,
                          const GTO<l4, m4, n4>& g4)
{
  return electronRepulsionIntegral1<l1, m1, n1, l2, m2, n2,
                                    l3, m3, n3, l4, m4, n4>(g1, g2, g3, g4);
}
//-----------------------------------------------------------------------------
} // End of namespace jdft

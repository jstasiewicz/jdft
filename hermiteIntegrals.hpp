#pragma once

#include <cmath>
#include <vector>

#include "contraction.hpp"
#include "generic.hpp"
#include "mpl.hpp"

/******************************************************************************
 * Calculation of Hermite Coulomb integrals, namely:
 *     \int_{\bf R^3} H_1(r) \frac{1}{|r-r_N|} H_2(r) dr
 * where H_1 and H_2 are Hermite-type orbitals and r_N is the nucleus position.
 * Also expansion of GTOs in HTOs is provided below.
 * Both runtime- and compile-time versions of recurrent formulas are provided.
 *****************************************************************************/
namespace jdft
{
//-----------------------------------------------------------------------------
///////////////////////////////BOYS FUNCTION///////////////////////////////////
//-----------------------------------------------------------------------------
fptype boysFunction(int n, fptype x)
{
  fptype F;

  if (x > .2F)
  {
    fptype ex = exp(-x);
    F = sqrt(M_PI_4 / x) * erf(::sqrt(x));
    for(int i = 1; i <= n; ++i)
      F = ((2*i-1) * F - ex) / (2.*x);

  }
  else
  {
    fptype twon = 2. * n;
    F  =  1./(twon+1. ) -  x
       * (1./(twon+3. ) - (x/2.)
       * (1./(twon+5. ) - (x/3.)
       * (1./(twon+7. ) - (x/4.)
       * (1./(twon+9. ) - (x/5.)
       * (1./(twon+11.) - (x/6.)/(twon+13.))))));
  }

  return F;
}
//-----------------------------------------------------------------------------
#pragma inline_recursion(on)
//-----------------------------------------------------------------------------
template<int i>
inline typename mpl_enable_fptype_if<i == 0>::type
boysHelper(const fptype x)
{
  return ::sqrtf(M_PI_4 / x) * erf(::sqrtf(x));
}
template<int i>
inline typename mpl_enable_fptype_if<(i > 0)>::type
boysHelper(const fptype x)
{
  return (static_cast<fptype>(2*i-1) * boysHelper<i-1>(x) - ::expf(-x)) / (2.F*x);
}

template<int n>
fptype boysFunction(fptype x)
{
  static const fptype twon = 2.F * static_cast<fptype>(n);

  if (x > .2F)
    return boysHelper<n>(x);
  else
    return  1.F/(twon+1.F ) -  x
              * (1.F/(twon+3.F ) - (x/2.F)
              * (1.F/(twon+5.F ) - (x/3.F)
              * (1.F/(twon+7.F ) - (x/4.F)
              * (1.F/(twon+9.F ) - (x/5.F)
              * (1.F/(twon+11.F) - (x/6.F)/(twon+13.F))))));
}
//-----------------------------------------------------------------------------
//////////////////////////////HERMITE EXPANSION////////////////////////////////
/////////////////////////////FORWARD DECLARATIONS//////////////////////////////
//-----------------------------------------------------------------------------
template<int i, int j, int t>
typename mpl_enable_fptype_if<
                              (t > 0) && (i > 0) &&
                              (t+1 > i+j) && (t <= i+j)
                             >::type
hermiteExpansion(fptype b1, fptype x1, fptype b2, fptype x2);
//-----------------------------------------------------------------------------
template<int i, int j, int t>
typename mpl_enable_fptype_if<
                              (t > 0) && (i > 0) &&
                              (t+2 > i+j) && (t+1 <= i+j)
                             >::type
hermiteExpansion(fptype b1, fptype x1, fptype b2, fptype x2);
//-----------------------------------------------------------------------------
template<int i, int j, int t>
typename mpl_enable_fptype_if<
                              (t > 0) && (i > 0) &&
                              (t+2 <= i+j)
                             >::type
hermiteExpansion(fptype b1, fptype x1, fptype b2, fptype x2);
//-----------------------------------------------------------------------------
template<int i, int j, int t>
typename mpl_enable_fptype_if<
                              (t > 0) && i == 0 && t == j
                             >::type
hermiteExpansion(fptype b1, fptype x1, fptype b2, fptype x2);

//-----------------------------------------------------------------------------
template<int i, int j, int t>
typename mpl_enable_fptype_if<
                              (t > 0) && i == 0 && (t < j)
                             >::type
hermiteExpansion(fptype b1, fptype x1, fptype b2, fptype x2);
//-----------------------------------------------------------------------------
//////////////////////////////HERMITE EXPANSION////////////////////////////////
/////////////////////////////////DEFINITIONS///////////////////////////////////
//-----------------------------------------------------------------------------
template<int i, int j, int t>
inline typename mpl_enable_fptype_if<
                                     (t < 0 || t > i+j)
                                    >::type
hermiteExpansion(fptype b1, fptype x1, fptype b2, fptype x2)
{
  return 0.;
}
//-----------------------------------------------------------------------------
template<int i, int j, int t>
inline typename mpl_enable_fptype_if<
                                     i == 0 && j == 0  && t == 0
                                    >::type
hermiteExpansion(fptype b1, fptype x1, fptype b2, fptype x2)
{
  const fptype bc     = b1 + b2,
               ab_x   = x2 - x1;

  return ::exp(-b1*b2*ab_x*ab_x/bc);
}
//-----------------------------------------------------------------------------
template<int i, int j, int t>
inline typename mpl_enable_fptype_if<
                                     t == 0 && i == 1 && j == 0
                                    >::type
hermiteExpansion(fptype b1, fptype x1, fptype b2, fptype x2)
{
  const fptype ab_x   = x2 - x1,
               bc     = b1 + b2,
               center = (b1*x1 + b2*x2)/bc;

  return ::exp(-b1*b2*ab_x*ab_x/bc)*(center-x1);
}
//-----------------------------------------------------------------------------
template<int i, int j, int t>
inline typename mpl_enable_fptype_if<
                                     i == 0 && t == 0 && j == 1
                                    >::type
hermiteExpansion(fptype b1, fptype x1, fptype b2, fptype x2)
{
  const fptype ab_x   = x2 - x1,
               bc     = b1 + b2,
               center = (b1*x1 + b2*x2)/bc;

  return ::exp(-b1*b2*ab_x*ab_x/bc)*(center-x2);
}
//-----------------------------------------------------------------------------
template<int i, int j, int t>
inline typename mpl_enable_fptype_if<
                                     i == 0 && t == 0 && (j > 1)
                                    >::type
hermiteExpansion(fptype b1, fptype x1, fptype b2, fptype x2)
{
  const fptype bc     = b1 + b2,
               center = (b1*x1 + b2*x2)/bc;

  return hermiteExpansion<0,j-1,1>(b1, x1, b2, x2) +
         hermiteExpansion<0,j-1,0>(b1, x1, b2, x2)*(center-x2);

}
//-----------------------------------------------------------------------------
template<int i, int j, int t>
inline typename mpl_enable_fptype_if<
                                     (t > 0) && i == 0 && t == j
                                    >::type
hermiteExpansion(fptype b1, fptype x1, fptype b2, fptype x2)
{
  const fptype bc = b1 + b2;

  return hermiteExpansion<0,j-1,t-1>(b1, x1, b2, x2)/(2.*bc);
}
//-----------------------------------------------------------------------------
template<int i, int j, int t>
inline typename mpl_enable_fptype_if<
                                     (t > 0) && i == 0 && (t < j)
                                    >::type
hermiteExpansion(fptype b1, fptype x1, fptype b2, fptype x2)
{
  const fptype bc     = b1 + b2,
               center = (b1*x1 + b2*x2)/bc;

  return hermiteExpansion<0,j-1,t+1>(b1, x1, b2, x2)*(t+1)       +
         hermiteExpansion<0,j-1,t  >(b1, x1, b2, x2)*(center-x2) +
         hermiteExpansion<0,j-1,t-1>(b1, x1, b2, x2)/(2*bc);
}
//-----------------------------------------------------------------------------
template<int i, int j, int t>
inline typename mpl_enable_fptype_if<
                                     (i > 0) && t == 0 &&
                                     (i != 1 || j != 0)
                                    >::type
hermiteExpansion(fptype b1, fptype x1, fptype b2, fptype x2)
{
  const fptype center = (b1*x1 + b2*x2)/(b1 + b2);

  return  hermiteExpansion<i-1,j,1>(b1, x1, b2, x2) +
          hermiteExpansion<i-1,j,0>(b1, x1, b2, x2)*(center-x1);
}
//-----------------------------------------------------------------------------
template<int i, int j, int t>
inline typename mpl_enable_fptype_if<
                                     (t > 0) && (i > 0) &&
                                     (t+1 > i+j) && (t <= i+j)
                                    >::type
hermiteExpansion(fptype b1, fptype x1, fptype b2, fptype x2)
{
  const fptype bc = b1 + b2;

  return hermiteExpansion<i-1,j,t-1>(b1, x1, b2, x2)/(2.*bc);
}
//-----------------------------------------------------------------------------
template<int i, int j, int t>
inline typename mpl_enable_fptype_if<
                                     (t > 0) && (i > 0) &&
                                     (t+2 > i+j) && (t+1 <= i+j)
                                    >::type
hermiteExpansion(fptype b1, fptype x1, fptype b2, fptype x2)
{
  const fptype bc     = b1 + b2,
               center = (b1*x1 + b2*x2)/bc;

  return hermiteExpansion<i-1,j,t  >(b1, x1, b2, x2)*(center-x1) +
         hermiteExpansion<i-1,j,t-1>(b1, x1, b2, x2)/(2.*bc);
}
//-----------------------------------------------------------------------------
template<int i, int j, int t>
inline typename mpl_enable_fptype_if<
                                     (t > 0) && (i > 0) && (t+2 <= i+j)
                                    >::type
hermiteExpansion(fptype b1, fptype x1, fptype b2, fptype x2)
{
  const fptype bc     = b1 + b2,
               center = (b1*x1 + b2*x2)/bc;

  return hermiteExpansion<i-1,j,t+1>(b1, x1, b2, x2)*(t+1)       +
         hermiteExpansion<i-1,j,t  >(b1, x1, b2, x2)*(center-x1) +
         hermiteExpansion<i-1,j,t-1>(b1, x1, b2, x2)/(2.*bc);
}
//-----------------------------------------------------------------------------
//////////////////////////////HERMITE EXPANSION////////////////////////////////
//////////////////////////////////FUNCTOR//////////////////////////////////////
//-----------------------------------------------------------------------------
template<int lmn1, int lmn2>
struct EvalHermiteExpansionFunctor
{
  EvalHermiteExpansionFunctor(fptype _b1, fptype _x1, fptype _b2, fptype _x2, fptype * const _E)
    : b1(_b1), x1(_x1), b2(_b2), x2(_x2), E(_E) {}

  template <int tuv>
  inline void eval()
  {
    E[tuv] = hermiteExpansion<lmn1,lmn2,tuv>(b1, x1, b2, x2);
  }
  const fptype b1, x1, b2, x2;
  fptype * const E;
};
//-----------------------------------------------------------------------------
///////////////////////////HERMITE COULOMB INTEGRALS///////////////////////////
/////////////////////////////FORWARD DECLARATIONS//////////////////////////////
//-----------------------------------------------------------------------------
template <int n, int t, int u, int v>
typename mpl_enable_fptype_if <
                               (t > 1) && (t+u+v > 3)
                              >::type
hermiteCoulombIntegral(fptype alpha, const vec3f& PC);
//-----------------------------------------------------------------------------
template <int n, int t, int u, int v>
typename mpl_enable_fptype_if <
                               t == 0 && (u > 1) && (t+u+v > 3)
                              >::type
hermiteCoulombIntegral(fptype alpha, const vec3f& PC);
//-----------------------------------------------------------------------------
template <int n, int t, int u, int v>
typename mpl_enable_fptype_if <
                               t == 0 && u == 0 && (v > 3)
                              >::type
hermiteCoulombIntegral(fptype alpha, const vec3f& PC);
//-----------------------------------------------------------------------------
template <int n, int t, int u, int v>
typename mpl_enable_fptype_if <
                               t == 0 && u == 1 && (v > 2)
                              >::type
hermiteCoulombIntegral(fptype alpha, const vec3f& PC);
//-----------------------------------------------------------------------------
///////////////////////////HERMITE COULOMB INTEGRALS///////////////////////////
/////////////////////////////////DEFINITIONS///////////////////////////////////
//-----------------------------------------------------------------------------
template <int n>
inline fptype hciBasic(fptype alpha, const vec3f& v)
{
  return pow(-2.*alpha, n) * boysFunction<n>(alpha*v.norm());
}
//-----------------------------------------------------------------------------
template <int n, int t, int u, int v>
typename mpl_enable_fptype_if <
                               t == 0 && u == 0 && v == 0
                              >::type
hermiteCoulombIntegral(fptype alpha, const vec3f& PC)
{
  return hciBasic<n>(alpha, PC);
}
//-----------------------------------------------------------------------------
template <int n, int t, int u, int v>
typename mpl_enable_fptype_if <
                               t == 1 && u == 0 && v == 0
                              >::type
hermiteCoulombIntegral(fptype alpha, const vec3f& PC)
{
  return PC.x * hciBasic<n+1>(alpha, PC);
}
//-----------------------------------------------------------------------------
template <int n, int t, int u, int v>
typename mpl_enable_fptype_if <
                               t == 0 && u == 1 && v == 0
                              >::type
hermiteCoulombIntegral(fptype alpha, const vec3f& PC)
{
  return PC.y * hciBasic<n+1>(alpha, PC);
}
//-----------------------------------------------------------------------------
template <int n, int t, int u, int v>
typename mpl_enable_fptype_if <
                               t == 0 && u == 0 && v == 1
                              >::type
hermiteCoulombIntegral(fptype alpha, const vec3f& PC)
{
  return PC.z * hciBasic<n+1>(alpha, PC);
}
//-----------------------------------------------------------------------------
template <int n, int t, int u, int v>
typename mpl_enable_fptype_if <
                               t == 1 && u == 1 && v == 0
                              >::type
hermiteCoulombIntegral(fptype alpha, const vec3f& PC)
{
  return PC.x * PC.y * hciBasic<n+2>(alpha, PC);
}
//-----------------------------------------------------------------------------
template <int n, int t, int u, int v>
typename mpl_enable_fptype_if <
                               t == 0 && u == 1 && v == 1
                              >::type
hermiteCoulombIntegral(fptype alpha, const vec3f& PC)
{
  return PC.y * PC.z * hciBasic<n+2>(alpha, PC);
}
//-----------------------------------------------------------------------------
template <int n, int t, int u, int v>
typename mpl_enable_fptype_if <
                               t == 1 && u == 0 && v == 1
                              >::type
hermiteCoulombIntegral(fptype alpha, const vec3f& PC)
{
  return PC.x * PC.z * hciBasic<n+2>(alpha, PC);
}
//-----------------------------------------------------------------------------
template <int n, int t, int u, int v>
typename mpl_enable_fptype_if <
                               t == 2 && u == 0 && v == 0
                              >::type
hermiteCoulombIntegral(fptype alpha, const vec3f& PC)
{
  return hciBasic<n+1>(alpha, PC)
           + PC.x*PC.x*hciBasic<n+2>(alpha, PC);
}
//-----------------------------------------------------------------------------
template <int n, int t, int u, int v>
typename mpl_enable_fptype_if <
                               t == 0 && u == 2 && v == 0
                              >::type
hermiteCoulombIntegral(fptype alpha, const vec3f& PC)
{
  return hciBasic<n+1>(alpha, PC)
           + PC.y*PC.y*hciBasic<n+2>(alpha, PC);
}
//-----------------------------------------------------------------------------
template <int n, int t, int u, int v>
typename mpl_enable_fptype_if <
                               t == 0 && u == 0 && v == 2
                              >::type
hermiteCoulombIntegral(fptype alpha, const vec3f& PC)
{
  return hciBasic<n+1>(alpha, PC)
           + PC.z*PC.z*hciBasic<n+2>(alpha, PC);
}
//-----------------------------------------------------------------------------
template <int n, int t, int u, int v>
typename mpl_enable_fptype_if <
                               t == 1 && u == 1 && v == 1
                              >::type
hermiteCoulombIntegral(fptype alpha, const vec3f& PC)
{
  return PC.x*PC.y*PC.z * hciBasic<n+3>(alpha, PC);
}
//-----------------------------------------------------------------------------
template <int n, int t, int u, int v>
typename mpl_enable_fptype_if <
                               t == 2 && u == 1 && v == 0
                              >::type
hermiteCoulombIntegral(fptype alpha, const vec3f& PC)
{
  return PC.y*hciBasic<n+2>(alpha, PC)
           + PC.x*PC.y*hciBasic<n+3>(alpha, PC);
}
//-----------------------------------------------------------------------------
template <int n, int t, int u, int v>
typename mpl_enable_fptype_if <
                               t == 2 && u == 0 && v == 1
                              >::type
hermiteCoulombIntegral(fptype alpha, const vec3f& PC)
{
  return PC.z*hciBasic<n+2>(alpha, PC)
           + PC.x*PC.z*hciBasic<n+3>(alpha, PC);
}
//-----------------------------------------------------------------------------
template <int n, int t, int u, int v>
typename mpl_enable_fptype_if <
                               t == 1 && u == 2 && v == 0
                              >::type
hermiteCoulombIntegral(fptype alpha, const vec3f& PC)
{
  return PC.x*hciBasic<n+2>(alpha, PC)
           + PC.x*PC.y*hciBasic<n+3>(alpha, PC);
}
//-----------------------------------------------------------------------------
template <int n, int t, int u, int v>
typename mpl_enable_fptype_if <
                               t == 0 && u == 2 && v == 1
                              >::type
hermiteCoulombIntegral(fptype alpha, const vec3f& PC)
{
  return PC.z*hciBasic<n+2>(alpha, PC)
           + PC.y*PC.z*hciBasic<n+3>(alpha, PC);
}
//-----------------------------------------------------------------------------
template <int n, int t, int u, int v>
typename mpl_enable_fptype_if <
                               t == 1 && u == 0 && v == 2
                              >::type
hermiteCoulombIntegral(fptype alpha, const vec3f& PC)
{
  return PC.x*hciBasic<n+2>(alpha, PC)
           + PC.x*PC.z*hciBasic<n+3>(alpha, PC);
}
//-----------------------------------------------------------------------------
template <int n, int t, int u, int v>
typename mpl_enable_fptype_if <
                               t == 0 && u == 1 && v == 2
                              >::type
hermiteCoulombIntegral(fptype alpha, const vec3f& PC)
{
  return PC.y*hciBasic<n+2>(alpha, PC)
           + PC.y*PC.z*hciBasic<n+3>(alpha, PC);
}
//-----------------------------------------------------------------------------
template <int n, int t, int u, int v>
typename mpl_enable_fptype_if <
                               t == 3 && u == 0 && v == 0
                              >::type
hermiteCoulombIntegral(fptype alpha, const vec3f& PC)
{
  return 3.*PC.x*hciBasic<n+2>(alpha, PC)
           + PC.x*PC.x*PC.x*hciBasic<n+3>(alpha, PC);
}
//-----------------------------------------------------------------------------
template <int n, int t, int u, int v>
typename mpl_enable_fptype_if <
                               t == 0 && u == 3 && v == 0
                              >::type
hermiteCoulombIntegral(fptype alpha, const vec3f& PC)
{
  return 3.*PC.y*hciBasic<n+2>(alpha, PC)
           + PC.y*PC.y*PC.y*hciBasic<n+3>(alpha, PC);
}
//-----------------------------------------------------------------------------
template <int n, int t, int u, int v>
typename mpl_enable_fptype_if <
                               t == 0 && u == 0 && v == 3
                              >::type
hermiteCoulombIntegral(fptype alpha, const vec3f& PC)
{
  return 3.*PC.z*hciBasic<n+2>(alpha, PC)
           + PC.z*PC.z*PC.z*hciBasic<n+3>(alpha, PC);
}
//-----------------------------------------------------------------------------
template <int n, int t, int u, int v>
typename mpl_enable_fptype_if <
                               t == 1 && (u+v > 2)
                              >::type
hermiteCoulombIntegral(fptype alpha, const vec3f& PC)
{
  return PC.x*hermiteCoulombIntegral<n+1,0,u,v>(alpha, PC);
}
//-----------------------------------------------------------------------------
template <int n, int t, int u, int v>
typename mpl_enable_fptype_if <
                               (t > 1) && (t+u+v > 3)
                              >::type
hermiteCoulombIntegral(fptype alpha, const vec3f& PC)
{
  return (t-1)*hermiteCoulombIntegral<n+1,t-2,u,v>(alpha, PC)
           + PC.x*hermiteCoulombIntegral<n+1,t-1,u,v>(alpha, PC);
}
//-----------------------------------------------------------------------------
template <int n, int t, int u, int v>
typename mpl_enable_fptype_if <
                               t == 0 && u == 1 && (v > 2)
                              >::type
hermiteCoulombIntegral(fptype alpha, const vec3f& PC)
{
  return PC.y*hermiteCoulombIntegral<n+1,0,0,v>(alpha, PC);
}
//-----------------------------------------------------------------------------
template <int n, int t, int u, int v>
typename mpl_enable_fptype_if <
                               t == 0 && (u > 1) && (t+u+v > 3)
                              >::type
hermiteCoulombIntegral(fptype alpha, const vec3f& PC)
{
  return (u-1)*hermiteCoulombIntegral<n+1,0,u-2,v>(alpha, PC)
           + PC.y*hermiteCoulombIntegral<n+1,0,u-1,v>(alpha, PC);
}
//-----------------------------------------------------------------------------
template <int n, int t, int u, int v>
typename mpl_enable_fptype_if <
                               t == 0 && u == 0 && (v > 3)
                              >::type
hermiteCoulombIntegral(fptype alpha, const vec3f& PC)
{
  return (v-1)*hermiteCoulombIntegral<n+1,0,0,v-2>(alpha, PC)
           + PC.z*hermiteCoulombIntegral<n+1,0,0,v-1>(alpha, PC);
}
//-----------------------------------------------------------------------------
///////////////////////////EVALUATOR (TRIPLE FOR LOOP)/////////////////////////
///////////////////////USED BY NUCLEAR ATTRACTION INTEGRAL/////////////////////
//-----------------------------------------------------------------------------
struct EvalHCI_NAI_Functor
{
  EvalHCI_NAI_Functor(fptype _alpha, const vec3f& _PC,
                      JDFT_RESTRICT fptype * const _E_til,
                      JDFT_RESTRICT fptype * const _E_ujm,
                      JDFT_RESTRICT fptype * const _E_vkn )
    : result(0.)
    , alpha(_alpha), PC(_PC), E_til(_E_til), E_ujm(_E_ujm), E_vkn(_E_vkn) {}

  template <int t, int u, int v>
  inline void eval()
  {
    result += E_til[t]*E_ujm[u]*E_vkn[v]
                * hermiteCoulombIntegral<0,t,u,v>(alpha, PC);
  }

  fptype result;

  fptype alpha;
  const vec3f& PC;
  JDFT_RESTRICT const fptype * const E_til;
  JDFT_RESTRICT const fptype * const E_ujm;
  JDFT_RESTRICT const fptype * const E_vkn;
};
//-----------------------------------------------------------------------------
///////////////////////////////TRIPLE FOR LOOP/////////////////////////////////
////////////////////USED BY ELECTRON REPULSION INTEGRAL////////////////////////
//-----------------------------------------------------------------------------
template <int twMax, int uxMax, int vyMax>
class HCI_Values
{
  public:
    template<int t, int u, int v>
    inline fptype& access()
    {
      return HCI_values[t*uxMax*vyMax + u*vyMax + v];
    }
    inline fptype& operator()(int t, int u, int v)
    {
      return HCI_values[t*uxMax*vyMax + u*vyMax + v];
    }
    template<int t, int u, int v>
    inline const fptype& access() const
    {
      return HCI_values[t*uxMax*vyMax + u*vyMax + v];
    }
    inline const fptype& operator()(int t, int u, int v) const
    {
      return HCI_values[t*uxMax*vyMax + u*vyMax + v];
    }
  private:
    JDFT_RESTRICT fptype HCI_values[twMax*uxMax*vyMax];
};

class HCI_Values_Dynamic
{
  public:
    HCI_Values_Dynamic(int _twMax, int _uxMax, int _vyMax)
      : twMax(_twMax), uxMax(_uxMax), vyMax(_vyMax)
      , HCI_values(_twMax*_uxMax*_vyMax) {}

    inline fptype& operator()(int t, int u, int v)
    {
      return HCI_values[t*uxMax*vyMax + u*vyMax + v];
    }
    inline const fptype& operator()(int t, int u, int v) const
    {
      return HCI_values[t*uxMax*vyMax + u*vyMax + v];
    }
  private:
    const int twMax, uxMax, vyMax;
    std::vector<fptype> HCI_values;
};
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
template <typename HCIValType>
struct EvalHCIFunctor
{
  EvalHCIFunctor(HCIValType& _HCI_values, fptype _alpha, const vec3f& _p1p2)
    : HCI_values(_HCI_values), alpha(_alpha), p1p2(_p1p2) {}

  template<int tw, int ux, int vy>
  inline void eval()
  {
    HCI_values.template access<tw,ux,vy>()
                             = hermiteCoulombIntegral<0,tw,ux,vy>(alpha, p1p2);
  }

  HCIValType& HCI_values;
  const fptype alpha;
  const vec3f& p1p2;
};
//-----------------------------------------------------------------------------
#pragma inline_recursion(off)
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
///////////////////////////RUNTIME RECURRENT VERSIONS//////////////////////////
///////////////////////////////////////////////////////////////////////////////
//-----------------------------------------------------------------------------
extern fptype qcinteg__gg_in_h(int i,fptype zetaA,fptype a_x, int j,fptype zetaB, fptype b_x, int t)
{

    fptype E, zetaC, center, ab_x;

    // HGTO exponent
    zetaC = (zetaA + zetaB);

    // HGTO center
    center = (zetaA * a_x + zetaB * b_x) / zetaC;

    ab_x = b_x - a_x;

    if(t < 0 || t > (i + j))
    {
        E = 0.0;
    }
    else
    {
        if(t==0 && i==0 && j==0)
  {
            E = exp(- zetaA * zetaB * ab_x*ab_x/(zetaA + zetaB));
        }
  else
  {
            if(i > 0) // i>0, j>=0, t>=0
      {
    if(t==0)
    {
        if(i==1 && j==0)
        {
      // E = qcinteg__gg_in_h(0,zetaA,a_x, 0,zetaB,b_x, 0) * (center - a_x);
      E = exp(- zetaA * zetaB * ab_x*ab_x/(zetaA + zetaB)) * (center - a_x);
        }
        else
        {
      E = qcinteg__gg_in_h(i-1,zetaA,a_x, j,zetaB,b_x, 1) +\
          qcinteg__gg_in_h(i-1,zetaA,a_x, j,zetaB,b_x, 0) * (center - a_x);
        }
    }
    else // i>0, j>=0, t>0
    {
        if((t+2) > (i+j))
        {
      if((t+1) > (i+j))
      {
          if(t > (i+j))
          {
        E = 0.0;
          }
          else
          {
        E = qcinteg__gg_in_h(i-1,zetaA,a_x, j,zetaB,b_x, t-1) / (2*zetaC);
          }
      }
      else
      {
          E = qcinteg__gg_in_h(i-1,zetaA,a_x, j,zetaB,b_x, t) * (center - a_x) +\
        qcinteg__gg_in_h(i-1,zetaA,a_x, j,zetaB,b_x, t-1) / (2*zetaC);
      }
        }
        else
        {
      E = qcinteg__gg_in_h(i-1,zetaA,a_x, j,zetaB,b_x, t+1) * (t+1) +\
          qcinteg__gg_in_h(i-1,zetaA,a_x, j,zetaB,b_x, t) * (center - a_x) +\
          qcinteg__gg_in_h(i-1,zetaA,a_x, j,zetaB,b_x, t-1) / (2*zetaC);
        }
    }
            }
      else // i==0, j>0
      {
                if(t == 0)
    {
        if(j==1)
        {
      // E = qcinteg__gg_in_h(0,zetaA,a_x, 0,zetaB,b_x, 0) * (center - b_x);
      E = exp(- zetaA * zetaB * ab_x*ab_x/(zetaA + zetaB)) * (center - b_x);
        }
        else // i==0, j>1
        {
      E = qcinteg__gg_in_h(0,zetaA,a_x, j-1,zetaB,b_x, 1) +\
          qcinteg__gg_in_h(0,zetaA,a_x, j-1,zetaB,b_x, 0) * (center - b_x);
        }
    }
    else // i==0, j>0, t>0
    {
        if(t > j)
        {
      E = 0.0;
        }
        else // i==0, j>0, 0 < t <= j
        {
      if(t==j)
      {
          E = qcinteg__gg_in_h(0,zetaA,a_x, j-1,zetaB,b_x, t-1) / (2*zetaC);
      }
      else // i==0, j>0, 0 < t < j
      {
          E = qcinteg__gg_in_h(0,zetaA,a_x, j-1,zetaB,b_x, t+1) * (t+1) +\
        qcinteg__gg_in_h(0,zetaA,a_x, j-1,zetaB,b_x, t) * (center - b_x) +\
        qcinteg__gg_in_h(0,zetaA,a_x, j-1,zetaB,b_x, t-1) / (2*zetaC);
      }
        }
                }
            }
        }
    }

    return E;
}

//-----------------------------------------------------------------------------
#include <stdlib.h> // calloc

#define hci000(n,alpha,x,y,z) (pow(-2*alpha,(n)) * boysFunction((n), alpha * (x*x + y*y + z*z)))

#define key_000 0
#define key_100 256
#define key_010 16
#define key_001 1
#define key_200 512
#define key_020 32
#define key_002 2
#define key_110 272
#define key_101 257
#define key_011 17
#define key_300 768
#define key_030 48
#define key_003 3
#define key_210 528
#define key_201 513
#define key_120 288
#define key_021 33
#define key_102 258
#define key_012 18
#define key_111 273


/**
 * Returns Hermite Coulomb Integral
 *
 * ie Rtuv^n(alpha, Rpc) in Helgaker
 *
 * n==0 for true HCI
 */
extern fptype qcinteg__hci(int n, int t,int u,int v, fptype alpha, fptype pc_x, fptype pc_y, fptype pc_z)
{
    fptype result;

    unsigned int key = 0;


    /* set key */
    if(t>14)
    {
  key |= 15; // ie at most 14 will be treated individually
    }
    else
    {
  key |= (unsigned int) t;
    }
    key <<= 4;
    if(u>14)
    {
  key |= 15;
    }
    else
    {
  key |= (unsigned int) u;
    }
    key <<= 4;
    if(v>14)
    {
  key |= 15;
    }
    else
    {
  key |= (unsigned int) v;
    }
    /* key now set to "tuv" */

    /* "Pattern matching" */
    switch (key)
    {
  case key_000:
      result = hci000(n, alpha, pc_x, pc_y, pc_z);
      break;
  case key_100:
      result = pc_x * hci000(n+1, alpha, pc_x, pc_y, pc_z);
      break;
  case key_010:
      result = pc_y * hci000(n+1, alpha, pc_x, pc_y, pc_z);
      break;
  case key_001:
      result = pc_z * hci000(n+1, alpha, pc_x, pc_y, pc_z);
      break;
  case key_200:
      result = hci000(n+1, alpha, pc_x, pc_y, pc_z) + pc_x*pc_x*hci000(n+2, alpha, pc_x, pc_y, pc_z);
      break;
  case key_020:
      result = hci000(n+1, alpha, pc_x, pc_y, pc_z) + pc_y*pc_y*hci000(n+2, alpha, pc_x, pc_y, pc_z);
      break;
  case key_002:
      result = hci000(n+1, alpha, pc_x, pc_y, pc_z) + pc_z*pc_z*hci000(n+2, alpha, pc_x, pc_y, pc_z);
      break;
  case key_110:
      result = pc_x * pc_y * hci000(n+2, alpha, pc_x, pc_y, pc_z);
      break;
  case key_101:
      result = pc_x * pc_z * hci000(n+2, alpha, pc_x, pc_y, pc_z);
      break;
  case key_011:
      result = pc_y * pc_z * hci000(n+2, alpha, pc_x, pc_y, pc_z);
      break;
  case key_300:
      result = 3*pc_x*hci000(n+2, alpha, pc_x, pc_y, pc_z) + pc_x*pc_x*pc_x*hci000(n+3, alpha, pc_x, pc_y, pc_z);
      break;
  case key_030:
      result = 3*pc_y*hci000(n+2, alpha, pc_x, pc_y, pc_z) + pc_y*pc_y*pc_y*hci000(n+3, alpha, pc_x, pc_y, pc_z);
      break;
  case key_003:
      result = 3*pc_z*hci000(n+2, alpha, pc_x, pc_y, pc_z) + pc_z*pc_z*pc_z*hci000(n+3, alpha, pc_x, pc_y, pc_z);
      break;
  case key_210:
      result = pc_y*hci000(n+2, alpha, pc_x, pc_y, pc_z) + pc_x*pc_y*hci000(n+3, alpha, pc_x, pc_y, pc_z);
      break;
  case key_201:
      result = pc_z*hci000(n+2, alpha, pc_x, pc_y, pc_z) + pc_x*pc_z*hci000(n+3, alpha, pc_x, pc_y, pc_z);
      break;
  case key_120:
      result = pc_x*hci000(n+2, alpha, pc_x, pc_y, pc_z) + pc_x*pc_y*hci000(n+3, alpha, pc_x, pc_y, pc_z);
      break;
  case key_021:
      result = pc_z*hci000(n+2, alpha, pc_x, pc_y, pc_z) + pc_y*pc_z*hci000(n+3, alpha, pc_x, pc_y, pc_z);
      break;
  case key_102:
      result = pc_x*hci000(n+2, alpha, pc_x, pc_y, pc_z) + pc_x*pc_z*hci000(n+3, alpha, pc_x, pc_y, pc_z);
      break;
  case key_012:
      result = pc_y*hci000(n+2, alpha, pc_x, pc_y, pc_z) + pc_y*pc_z*hci000(n+3, alpha, pc_x, pc_y, pc_z);
      break;
  case key_111:
      result = pc_x * pc_y * pc_z * hci000(n+3, alpha, pc_x, pc_y, pc_z);
  default:
      if(t > 0)
      {
    if(t==1)
    {
        result = pc_x * qcinteg__hci(n+1, 0,u,v, alpha, pc_x, pc_y, pc_z);
    }
    else // t>1
    {
        result = (t-1)*qcinteg__hci(n+1, t-2,u,v, alpha, pc_x, pc_y, pc_z) +\
      pc_x*qcinteg__hci(n+1, t-1,u,v, alpha, pc_x, pc_y, pc_z);
    }
      }
      else // t==0
      {
    if(u > 0)
    {
        if(u==1)
        {
      result = pc_y * qcinteg__hci(n+1, 0,0,v, alpha, pc_x, pc_y, pc_z);
        }
        else
        {
      result = (u-1)*qcinteg__hci(n+1, 0,u-2,v, alpha, pc_x, pc_y, pc_z) +\
          pc_y*qcinteg__hci(n+1, 0,u-1,v, alpha, pc_x, pc_y, pc_z);
        }
    }
    else // t==0, u==0, v>3
    {
        result = (v-1)*qcinteg__hci(n+1, 0,0,v-2, alpha, pc_x, pc_y, pc_z) +\
      pc_z*qcinteg__hci(n+1, 0,0,v-1, alpha, pc_x, pc_y, pc_z);
    }
      }
      break;
    }

    return result;
}
//-----------------------------------------------------------------------------
} // End of namespace jdft

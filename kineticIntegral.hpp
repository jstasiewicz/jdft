#pragma once

#include "generic.hpp"
#include "utils.hpp"
#include "contraction.hpp"
#include "overlapIntegral.hpp"
#include "mpl.hpp"

namespace jdft
{
//-----------------------------------------------------------------------------
template <int l1, int m1, int n1,
          int l2, int m2, int n2>
fptype kineticEnergyIntegral(const GTO<l1,m1,n1>& g1, const GTO<l2,m2,n2>& g2)
{
  fptype fl1 = static_cast<fptype>(l1),
         fl2 = static_cast<fptype>(l2),
         fm1 = static_cast<fptype>(m1),
         fm2 = static_cast<fptype>(m2),
         fn1 = static_cast<fptype>(n1),
         fn2 = static_cast<fptype>(n2);

  return   .5* fl1 * fl2  * overlapIntegralBase<l1-1,m1,n1,l2-1,m2,n2>(g1, g2)
         + 2.*g1.b * g2.b * overlapIntegralBase<l1+1,m1,n1,l2+1,m2,n2>(g1, g2)
         -    g1.b * fl2  * overlapIntegralBase<l1+1,m1,n1,l2-1,m2,n2>(g1, g2)
         -     fl1 * g2.b * overlapIntegralBase<l1-1,m1,n1,l2+1,m2,n2>(g1, g2)
         + .5* fm1 * fm2  * overlapIntegralBase<l1,m1-1,n1,l2,m2-1,n2>(g1, g2)
         + 2.*g1.b * g2.b * overlapIntegralBase<l1,m1+1,n1,l2,m2+1,n2>(g1, g2)
         -    g1.b * fm2  * overlapIntegralBase<l1,m1+1,n1,l2,m2-1,n2>(g1, g2)
         -     fm1 * g2.b * overlapIntegralBase<l1,m1-1,n1,l2,m2+1,n2>(g1, g2)
         + .5* fn1 * fn2  * overlapIntegralBase<l1,m1,n1-1,l2,m2,n2-1>(g1, g2)
         + 2.*g1.b * g2.b * overlapIntegralBase<l1,m1,n1+1,l2,m2,n2+1>(g1, g2)
         -    g1.b * fn2  * overlapIntegralBase<l1,m1,n1+1,l2,m2,n2-1>(g1, g2)
         -     fn1 * g2.b * overlapIntegralBase<l1,m1,n1-1,l2,m2,n2+1>(g1, g2);
}
//-----------------------------------------------------------------------------

} // End of namespace jdft

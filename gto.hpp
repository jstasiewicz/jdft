#pragma once

#include <tuple>

#include <string>
#include <iostream>

#include "generic.hpp"
#include "utils.hpp"

namespace jdft
{
//-----------------------------------------------------------------------------
struct GTOBase
{
  GTOBase(fptype _b, fptype _coef, vec3f _center)
    : b(_b), coef(_coef), center(_center) {}

  virtual ~GTOBase() {}

  void move(const vec3f& vec)
  {center += vec;}

  virtual void getCartesianPowers(int &l_, int &m_, int &n_) const = 0;
  virtual std::string print() const = 0;

  fptype b, coef;
  vec3f center;

  private:
    GTOBase();
};
//-----------------------------------------------------------------------------
template <int _l, int _m, int _n>
struct GTO : public GTOBase
{
  GTO(fptype _b, fptype _coef, vec3f _center)
    : GTOBase(_b, _coef, _center) {}

  fptype operator()(const vec3f& pos) const;
  virtual void getCartesianPowers(int &l_, int &m_, int &n_) const;
  virtual std::string print() const;

  static const int l = _l,
                   m = _m,
                   n = _n;

  private:
    GTO();
};

template <int _l, int _m, int _n>
fptype GTO<_l,_m,_n>::operator()(const vec3f& pos) const
{
  vec3f distanceFromCenter = (pos-center);
  return ::pow(distanceFromCenter.x, l)
            * ::pow(distanceFromCenter.y, m)
            * ::pow(distanceFromCenter.z, n)
            * coef*::exp(-b*distanceFromCenter.norm());
}
template <int _l, int _m, int _n>
void GTO<_l,_m,_n>::getCartesianPowers(int &l_, int &m_, int &n_) const
{
  l_ = _l;
  m_ = _m;
  n_ = _n;
}
template <int _l, int _m, int _n>
std::string GTO<_l,_m,_n>::print() const
{
    return std::string() + "GTO [l=" + toString(_l)
                          +    ", m=" + toString(_m)
                          +    ", n=" + toString(_n) + "]"
            + " with width=" + toString(b) + " and coefficient=" + toString(coef)
            + " centered at " + center.print();
}
//-----------------------------------------------------------------------------
typedef GTO<0,0,0> G000;

typedef GTO<1,0,0> G100;
typedef GTO<0,1,0> G010;
typedef GTO<0,0,1> G001;

typedef GTO<2,0,0> G200;
typedef GTO<0,2,0> G020;
typedef GTO<0,0,2> G002;
typedef GTO<1,1,0> G110;
typedef GTO<0,1,1> G011;
typedef GTO<1,0,1> G101;

typedef GTO<3,0,0> G300;
typedef GTO<0,3,0> G030;
typedef GTO<0,0,3> G003;
typedef GTO<2,1,0> G210;
typedef GTO<1,2,0> G120;
typedef GTO<0,1,2> G012;
typedef GTO<2,0,1> G201;
typedef GTO<0,2,1> G021;
typedef GTO<1,0,2> G102;
typedef GTO<1,1,1> G111;

typedef GTO<4,0,0> G400;
typedef GTO<0,4,0> G040;
typedef GTO<0,0,4> G004;
typedef GTO<3,1,0> G310;
typedef GTO<1,3,0> G130;
typedef GTO<1,0,3> G103;
typedef GTO<3,0,1> G301;
typedef GTO<0,3,1> G031;
typedef GTO<0,1,3> G013;
typedef GTO<2,2,0> G220;
typedef GTO<0,2,2> G022;
typedef GTO<2,0,2> G202;
typedef GTO<2,1,1> G211;
typedef GTO<1,2,1> G121;
typedef GTO<1,1,2> G112;
//-----------------------------------------------------------------------------
} // End of namespace jdft

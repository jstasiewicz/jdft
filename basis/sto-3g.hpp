#pragma once

#include "basis/basisGeneric.hpp"

#include "generic.hpp"
#include "utils.hpp"
#include "contraction.hpp"

#include <array>
#include <memory>

namespace jdft
{
namespace sto_3g
{
//-----------------------------------------------------------------------------
struct H : public AtomIntermediate<H, Elements::H>
{
    typedef AtomIntermediate<H, Elements::H> AtomIntermediateType;

    explicit H(const vec3f& _nucleusPosition)
      : AtomIntermediateType(_nucleusPosition)
    {
        basisFuns.assign({
                    {
                      std::make_shared<G000>(3.42525091, 0.15432897, _nucleusPosition),
                      std::make_shared<G000>(0.62391373, 0.53532814, _nucleusPosition),
                      std::make_shared<G000>(0.16885540, 0.44463454, _nucleusPosition)
                    }
                  });
    }

    static const Elements   element   = Elements::H;
    static const BasisTypes basisType = BasisTypes::STO3G;

  private:
    virtual std::string print() const
    {
      return AtomIntermediateType::print();
    }
};
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
} } // End of namespace jdft::sto_3g

#pragma once

#include "generic.hpp"
#include "utils.hpp"
#include "contraction.hpp"

#include <iostream>
#include <sstream>
#include <string>

#include <array>
#include <memory>

namespace jdft
{
//-----------------------------------------------------------------------------
enum class Elements
{
  H = 0,                                                              He,
  Li, Be,                                         B,  C,  N,  O,  F,  Ne,
  Na, Mg,                                         Al, Si, P,  S,  Cl, Ar,
  K,  Ca, Sc, Ti, V,  Cr, Mn, Fe, Co, Ni, Cu, Zn, Ga, Ge, As, Se, Br, Kr,
  Rb, Sr, Y,  Zr, Nb, Mo, Tc, Ru, Rh, Pd, Ag, Cd, In, Sn, Sb, Te, I,  Xe,
  LAST_ELEMENT
};
//-----------------------------------------------------------------------------
template <Elements elem>
struct NumElectrons
{
  static const size_t value = size_t(elem) + 1u;
};
//-----------------------------------------------------------------------------
static constexpr std::array<const char*, size_t(Elements::LAST_ELEMENT)> atomNames =
{{
  "Hydrogen", "Helium",

  "Lithium", "Berillium", "Boron", "Carbon", "Nitrogen", "Oxygen", "Fluorine", "Neon",

  "Sodium", "Magnesium", "Aluminium", "Silicon", "Phosphorus", "Sulfur", "Chlorine", "Argon",

  "Potassium", "Calcium", "Scandium", "Titanium", "Vanadium", "Chromium", "Manganese", "Iron", "Cobalt",
  "Nickel", "Copper", "Zinc", "Galium", "Germanium", "Arsenium", "Selenium", "Bromine", "Krypton",

  "Rubidium", "Strontium", "Yttrium", "Zirconium", "Niobium", "Molybdenum", "Technetium", "Ruthenium",
  "Rhodium", "Palladium", "Silver", "Cadmmium", "Indium", "Tin", "Antimony", "Tellurium", "Iodine", "Xenon"
}};
//-----------------------------------------------------------------------------
enum class BasisTypes
{
    STO3G, STO6G, _631Gx, _6311G, _6311pGx
};
//-----------------------------------------------------------------------------
struct BasisAtom
{
    virtual ~BasisAtom() {}
    virtual BasisTypes reflectBasisType() const = 0;
    virtual Elements   reflectElement()   const = 0;

    typedef std::vector<Contraction> BasisFunVector;
    virtual       BasisFunVector& getBasisFuns()       = 0;
    virtual const BasisFunVector& getBasisFuns() const = 0;

    virtual fptype getNuclearCharge() const = 0;
    virtual const vec3f& getNucleusPosition() const = 0;

    virtual size_t getNumBasisFuns() const = 0;

    friend std::ostream& operator<<(std::ostream& os, const BasisAtom& ba);

  private:
    virtual std::string print() const = 0;
};
std::ostream& operator<<(std::ostream& os, const BasisAtom& ba)
{
    return os << ba.print();
}
//-----------------------------------------------------------------------------
template<typename Derived, Elements element>
struct AtomIntermediate : public BasisAtom
{
    typedef Derived MyType;
    virtual ~AtomIntermediate<Derived, element>() { }
    virtual BasisTypes reflectBasisType() const
    {
        return Derived::basisType;
    }
    virtual Elements  reflectElement()  const
    {
        return element;
    }
    //static constexpr char* name = std::get<size_t(element)>(atomNames);

    void move(const vec3f &_vec)
    {
      nucleusPosition += _vec;
      for(auto basisFun : basisFuns)
        basisFun->move(_vec);
    }
    virtual const vec3f& getNucleusPosition() const
    {
      return nucleusPosition;
    }

    virtual       BasisFunVector& getBasisFuns()       { return basisFuns; }
    virtual const BasisFunVector& getBasisFuns() const { return basisFuns; }

    virtual fptype getNuclearCharge() const
    { return static_cast<fptype>(NumElectrons<element>::value); }

    virtual size_t getNumBasisFuns() const { return basisFuns.size(); }

  protected:
    explicit AtomIntermediate<Derived, element>(const vec3f &_nucleusPosition)
     : nucleusPosition(_nucleusPosition) {}

    std::string print() const
    {
      std::ostringstream oss;
      oss << std::get<size_t(element)>(atomNames) << " atom" << std::endl
          << '{' << std::endl;
      for(auto basisFun : basisFuns)
        oss << basisFun;
      oss << '}' << std::endl;
      return oss.str();
    }

    BasisFunVector basisFuns;
    vec3f nucleusPosition;

};
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
} // End of namespace jdft.

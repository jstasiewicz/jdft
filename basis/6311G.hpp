#pragma once

#include "basis/basisGeneric.hpp"

#include "generic.hpp"
#include "utils.hpp"
#include "contraction.hpp"

#include <array>
#include <memory>

namespace jdft
{
//-----------------------------------------------------------------------------
namespace _6311G
{
//-----------------------------------------------------------------------------
struct H : public AtomIntermediate<H, Elements::H>
{
    typedef AtomIntermediate<H, Elements::H> AtomIntermediateType;

    explicit H(const vec3f& _nucleusPosition)
      : AtomIntermediateType(_nucleusPosition)
    {
        basisFuns.assign({
                    {
                      std::make_shared<G000>(33.865000, 0.0254938, _nucleusPosition),
                      std::make_shared<G000>(5.0947900, 0.1903730, _nucleusPosition),
                      std::make_shared<G000>(1.1587900, 0.8521610, _nucleusPosition)
                    },
                    {
                      std::make_shared<G000>(0.3258400, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G000>(0.1027410, 1.0000000, _nucleusPosition)
                    }
                  });
    }

    static const Elements   element   = Elements::H;
    static const BasisTypes basisType = BasisTypes::_6311G;

  private:
    virtual std::string print() const
    {
      return AtomIntermediateType::print();
    }
};
//-----------------------------------------------------------------------------
} } // End of namespace jdft::_6311G

#pragma once

#include "basis/basisGeneric.hpp"
#include "basis/6311G.hpp"

#include "generic.hpp"
#include "utils.hpp"
#include "contraction.hpp"

#include <array>
#include <memory>

namespace jdft
{
namespace _6311pGx
{
//-----------------------------------------------------------------------------
typedef _6311G::H H; // We use hydrogen from 6-311G basis here
//-----------------------------------------------------------------------------
struct C : public AtomIntermediate<C, Elements::C>
{
    typedef AtomIntermediate<C, Elements::C> AtomIntermediateType;

    explicit C(const vec3f& _nucleusPosition)
      : AtomIntermediateType(_nucleusPosition)
    {
        basisFuns.assign({
                    {
                      std::make_shared<G000>(4563.2400, 0.00196665, _nucleusPosition),
                      std::make_shared<G000>(682.02400, 0.0152306,  _nucleusPosition),
                      std::make_shared<G000>(154.97300, 0.0761269,  _nucleusPosition),
                      std::make_shared<G000>(44.455300, 0.2608010,  _nucleusPosition),
                      std::make_shared<G000>(13.029000, 0.6164620,  _nucleusPosition),
                      std::make_shared<G000>(1.8277300, 0.2210060,  _nucleusPosition)
                    },
                    {
                      std::make_shared<G000>(20.964200, 0.1146600,   _nucleusPosition),
                      std::make_shared<G000>(4.8033100, 0.9199990,   _nucleusPosition),
                      std::make_shared<G000>(1.4593300, -0.00303068, _nucleusPosition)
                    },
                    {
                      std::make_shared<G000>(0.4834560, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G000>(0.1455850, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G000>(0.0438000, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G100>(20.964200, 0.0402487, _nucleusPosition),
                      std::make_shared<G100>(4.8033100, 0.2375940, _nucleusPosition),
                      std::make_shared<G100>(1.4593300, 0.8158540, _nucleusPosition)
                    },
                    {
                      std::make_shared<G010>(20.964200, 0.0402487, _nucleusPosition),
                      std::make_shared<G010>(4.8033100, 0.2375940, _nucleusPosition),
                      std::make_shared<G010>(1.4593300, 0.8158540, _nucleusPosition)
                    },
                    {
                      std::make_shared<G001>(20.964200, 0.0402487, _nucleusPosition),
                      std::make_shared<G001>(4.8033100, 0.2375940, _nucleusPosition),
                      std::make_shared<G001>(1.4593300, 0.8158540, _nucleusPosition)
                    },
                    {
                      std::make_shared<G100>(0.4834560, 1.0000000, _nucleusPosition),
                    },
                    {
                      std::make_shared<G010>(0.4834560, 1.0000000, _nucleusPosition),
                    },
                    {
                      std::make_shared<G001>(0.4834560, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G100>(0.1455850, 1.0000000, _nucleusPosition),
                    },
                    {
                      std::make_shared<G010>(0.1455850, 1.0000000, _nucleusPosition),
                    },
                    {
                      std::make_shared<G001>(0.1455850, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G100>(0.0438000, 1.0000000, _nucleusPosition),
                    },
                    {
                      std::make_shared<G010>(0.0438000, 1.0000000, _nucleusPosition),
                    },
                    {
                      std::make_shared<G001>(0.0438000, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G110>(0.6260000, 1.0000000, _nucleusPosition),
                    },
                    {
                      std::make_shared<G011>(0.6260000, 1.0000000, _nucleusPosition),
                    },
                    {
                      std::make_shared<G101>(0.6260000, 1.0000000, _nucleusPosition),
                    },
                    {
                      std::make_shared<G200>(0.6260000, 1.0000000, _nucleusPosition),
                    },
                    {
                      std::make_shared<G020>(0.6260000, 1.0000000, _nucleusPosition),
                    },
                    {
                      std::make_shared<G002>(0.6260000, 1.0000000, _nucleusPosition)
                    }
                  });
    }

    static const Elements   element   = Elements::C;
    static const BasisTypes basisType = BasisTypes::_6311pGx;

  private:
    virtual std::string print() const
    { return AtomIntermediateType::print(); }
};
//-----------------------------------------------------------------------------
struct N : public AtomIntermediate<N, Elements::N>
{
    typedef AtomIntermediate<N, Elements::N> AtomIntermediateType;

    explicit N(const vec3f& _nucleusPosition)
      : AtomIntermediateType(_nucleusPosition)
    {
        basisFuns.assign({
                    {
                      std::make_shared<G000>(6293.4800, 0.00196979, _nucleusPosition),
                      std::make_shared<G000>(949.04400, 0.0149613,  _nucleusPosition),
                      std::make_shared<G000>(218.77600, 0.0735006,  _nucleusPosition),
                      std::make_shared<G000>(63.691600, 0.2489370,  _nucleusPosition),
                      std::make_shared<G000>(18.828200, 0.6024600,  _nucleusPosition),
                      std::make_shared<G000>(2.7202300, 0.2562020,  _nucleusPosition)
                    },
                    {
                      std::make_shared<G000>(30.633100, 0.1119060,   _nucleusPosition),
                      std::make_shared<G000>(7.0261400, 0.9216660,   _nucleusPosition),
                      std::make_shared<G000>(2.1120500, -0.00256919, _nucleusPosition)
                    },
                    {
                      std::make_shared<G000>(0.6840090, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G000>(0.2008780, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G000>(0.0639000, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G100>(30.633100, 0.0383119, _nucleusPosition),
                      std::make_shared<G100>(7.0261400, 0.2374030, _nucleusPosition),
                      std::make_shared<G100>(2.1120500, 0.8175920, _nucleusPosition)
                    },
                    {
                      std::make_shared<G010>(30.633100, 0.0383119, _nucleusPosition),
                      std::make_shared<G010>(7.0261400, 0.2374030, _nucleusPosition),
                      std::make_shared<G010>(2.1120500, 0.8175920, _nucleusPosition)
                    },
                    {
                      std::make_shared<G001>(30.633100, 0.0383119, _nucleusPosition),
                      std::make_shared<G001>(7.0261400, 0.2374030, _nucleusPosition),
                      std::make_shared<G001>(2.1120500, 0.8175920, _nucleusPosition)
                    },
                    {
                      std::make_shared<G100>(0.6840090, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G010>(0.6840090, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G001>(0.6840090, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G100>(0.2008780, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G010>(0.2008780, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G001>(0.2008780, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G100>(0.0639000, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G010>(0.0639000, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G001>(0.0639000, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G110>(0.9130000, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G011>(0.9130000, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G101>(0.9130000, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G200>(0.9130000, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G020>(0.9130000, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G002>(0.9130000, 1.0000000, _nucleusPosition)
                    }
                  });
    }

    static const Elements   element   = Elements::N;
    static const BasisTypes basisType = BasisTypes::_6311pGx;

  private:
    virtual std::string print() const
    { return AtomIntermediateType::print(); }
};
//-----------------------------------------------------------------------------
struct O : public AtomIntermediate<O, Elements::O>
{
    typedef AtomIntermediate<O, Elements::O> AtomIntermediateType;

    explicit O(const vec3f& _nucleusPosition)
      : AtomIntermediateType(_nucleusPosition)
    {
        basisFuns.assign({
                    {
                      std::make_shared<G000>(8588.5000, 0.00189515, _nucleusPosition),
                      std::make_shared<G000>(1297.2300, 0.0143859,  _nucleusPosition),
                      std::make_shared<G000>(299.29600, 0.0707320,  _nucleusPosition),
                      std::make_shared<G000>(87.377100, 0.2400010,  _nucleusPosition),
                      std::make_shared<G000>(25.678900, 0.5947970,  _nucleusPosition),
                      std::make_shared<G000>(3.7400400, 0.2808020,  _nucleusPosition)
                    },
                    {
                      std::make_shared<G000>(42.117500, 0.1138890,   _nucleusPosition),
                      std::make_shared<G000>(9.6283700, 0.9208110,   _nucleusPosition),
                      std::make_shared<G000>(2.8533200, -0.00327447, _nucleusPosition)
                    },
                    {
                      std::make_shared<G000>(0.9056610, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G000>(0.2556110, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G000>(0.0845000, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G100>(42.117500, 0.0365114, _nucleusPosition),
                      std::make_shared<G100>(9.6283700, 0.2371530, _nucleusPosition),
                      std::make_shared<G100>(2.8533200, 0.8197020, _nucleusPosition)
                    },
                    {
                      std::make_shared<G010>(42.117500, 0.0365114, _nucleusPosition),
                      std::make_shared<G010>(9.6283700, 0.2371530, _nucleusPosition),
                      std::make_shared<G010>(2.8533200, 0.8197020, _nucleusPosition)
                    },
                    {
                      std::make_shared<G001>(42.117500, 0.0365114, _nucleusPosition),
                      std::make_shared<G001>(9.6283700, 0.2371530, _nucleusPosition),
                      std::make_shared<G001>(2.8533200, 0.8197020, _nucleusPosition)
                    },
                    {
                      std::make_shared<G100>(0.9056610, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G010>(0.9056610, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G001>(0.9056610, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G100>(0.2556110, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G010>(0.2556110, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G001>(0.2556110, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G100>(0.0845000, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G010>(0.0845000, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G001>(0.0845000, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G110>(1.2920000, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G011>(1.2920000, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G101>(1.2920000, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G200>(1.2920000, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G020>(1.2920000, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G002>(1.2920000, 1.0000000, _nucleusPosition)
                    }
                  });
    }

    static const Elements   element   = Elements::O;
    static const BasisTypes basisType = BasisTypes::_6311pGx;

  private:
    virtual std::string print() const
    { return AtomIntermediateType::print(); }
};
//-----------------------------------------------------------------------------
struct P : public AtomIntermediate<P, Elements::P>
{
    typedef AtomIntermediate<P, Elements::P> AtomIntermediateType;

    explicit P(const vec3f& _nucleusPosition)
      : AtomIntermediateType(_nucleusPosition)
    {
        basisFuns.assign({
                    {
                      std::make_shared<G000>(77492.4000, 0.0007810, _nucleusPosition),
                      std::make_shared<G000>(11605.8000, 0.0060680, _nucleusPosition),
                      std::make_shared<G000>(2645.96000, 0.0311600, _nucleusPosition),
                      std::make_shared<G000>(754.976000, 0.1234310, _nucleusPosition),
                      std::make_shared<G000>(248.755000, 0.3782090, _nucleusPosition),
                      std::make_shared<G000>(91.1565000, 0.5632620, _nucleusPosition)
                    },
                    {
                      std::make_shared<G000>(91.1565000, 0.1602550, _nucleusPosition),
                      std::make_shared<G000>(36.2257000, 0.6276470, _nucleusPosition),
                      std::make_shared<G000>(15.2113000, 0.2638490, _nucleusPosition)
                    },
                    {
                      std::make_shared<G000>(4.7941700, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G000>(1.8079300, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G000>(0.3568160, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G000>(0.1147830, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G000>(0.0348000, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G100>(384.843000, 0.0092060, _nucleusPosition),
                      std::make_shared<G100>(90.5521000, 0.0698740, _nucleusPosition),
                      std::make_shared<G100>(29.1339000, 0.2924700, _nucleusPosition),
                      std::make_shared<G100>(10.8862000, 0.7281030, _nucleusPosition)
                    },
                    {
                      std::make_shared<G010>(384.843000, 0.0092060, _nucleusPosition),
                      std::make_shared<G010>(90.5521000, 0.0698740, _nucleusPosition),
                      std::make_shared<G010>(29.1339000, 0.2924700, _nucleusPosition),
                      std::make_shared<G010>(10.8862000, 0.7281030, _nucleusPosition)
                    },
                    {
                      std::make_shared<G001>(384.843000, 0.0092060, _nucleusPosition),
                      std::make_shared<G001>(90.5521000, 0.0698740, _nucleusPosition),
                      std::make_shared<G001>(29.1339000, 0.2924700, _nucleusPosition),
                      std::make_shared<G001>(10.8862000, 0.7281030, _nucleusPosition)
                    },
                    {
                      std::make_shared<G100>(4.3525900, 0.6283490, _nucleusPosition),
                      std::make_shared<G100>(1.7770600, 0.4280440, _nucleusPosition)
                    },
                    {
                      std::make_shared<G010>(4.3525900, 0.6283490, _nucleusPosition),
                      std::make_shared<G010>(1.7770600, 0.4280440, _nucleusPosition)
                    },
                    {
                      std::make_shared<G001>(4.3525900, 0.6283490, _nucleusPosition),
                      std::make_shared<G001>(1.7770600, 0.4280440, _nucleusPosition)
                    },
                    {
                      std::make_shared<G100>(0.6970050, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G010>(0.6970050, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G001>(0.6970050, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G100>(0.2535320, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G010>(0.2535320, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G001>(0.2535320, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G100>(0.0684930, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G010>(0.0684930, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G001>(0.0684930, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G100>(0.0348000, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G010>(0.0348000, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G001>(0.0348000, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G110>(0.5500000, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G011>(0.5500000, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G101>(0.5500000, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G200>(0.5500000, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G020>(0.5500000, 1.0000000, _nucleusPosition)
                    },
                    {
                      std::make_shared<G002>(0.5500000, 1.0000000, _nucleusPosition)
                    }
                  });
    }

    static const Elements   element   = Elements::P;
    static const BasisTypes basisType = BasisTypes::_6311pGx;

  private:
    virtual std::string print() const
    { return AtomIntermediateType::print(); }
};
//-----------------------------------------------------------------------------

} } // End of namespace jdft::_6311pGx

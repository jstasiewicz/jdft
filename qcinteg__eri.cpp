/* qcinteg__eri.c
 *
 * Electron Repulsion Integral
 */

/*
    Copyright 2008, 2009, Jan Šmydke
     All rights reserved.

    This file is a part of "qcinteg"

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

      1. Redistributions of source code must retain the above copyright
         notice, this list of conditions and the following disclaimer.
      2. Redistributions in binary form must reproduce the above copyright
         notice, this list of conditions and the following disclaimer in the
         documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h> // calloc
#include <iostream>
#include <math.h>

#include "qcinteg__boys.h"
#include "qcinteg__gg_in_h.h"
#include "qcinteg__hci.h"

/**
 * Electron Repulsion Integral
 * of four 3Dim primitive GTOs
 *
 * GTOs centered on A, B, C, D
 */
extern double qcinteg__eri(int *ijkA, double zetaA, double *xyzA, int *ijkB, double zetaB, double *xyzB, int *ijkC, double zetaC, double *xyzC, int *ijkD, double zetaD, double *xyzD)
{
    double result;

    double zetaP1, zetaP2;
    double alpha;

    double p1_x, p1_y, p1_z, p2_x, p2_y, p2_z, p1p2_x, p1p2_y, p1p2_z;

    double *e_t_il, *e_u_jm, *e_v_kn, *e_tau_or, *e_nu_ps, *e_phi_qt;

    double part1;

    int tt, uu, vv, tau, nu, phi;


    e_t_il = (double*) calloc(ijkA[0]+ijkB[0] + ijkA[1]+ijkB[1] + ijkA[2]+ijkB[2] + ijkC[0]+ijkD[0] + ijkC[1]+ijkD[1] + ijkC[2]+ijkD[2] + 6, sizeof(double));
    e_u_jm = e_t_il + ijkA[0] + ijkB[0] + 1;
    e_v_kn = e_u_jm + ijkA[1] + ijkB[1] + 1;
    e_tau_or = e_v_kn + ijkA[2] + ijkB[2] + 1;
    e_nu_ps = e_tau_or + ijkC[0] + ijkD[0] + 1;
    e_phi_qt = e_nu_ps + ijkC[1] + ijkD[1] + 1;

    // HGTO exponents
    zetaP1 = zetaA + zetaB;
    zetaP2 = zetaC + zetaD;
    alpha = zetaP1 * zetaP2 / (zetaP1 + zetaP2);

    // HGTO centers
    p1_x = (zetaA*xyzA[0] + zetaB*xyzB[0]) / zetaP1;
    p1_y = (zetaA*xyzA[1] + zetaB*xyzB[1]) / zetaP1;
    p1_z = (zetaA*xyzA[2] + zetaB*xyzB[2]) / zetaP1;
    p2_x = (zetaC*xyzC[0] + zetaD*xyzD[0]) / zetaP2;
    p2_y = (zetaC*xyzC[1] + zetaD*xyzD[1]) / zetaP2;
    p2_z = (zetaC*xyzC[2] + zetaD*xyzD[2]) / zetaP2;
    p1p2_x = (p1_x - p2_x); // !!! here Rab = a - b
    p1p2_y = (p1_y - p2_y); // !!! and not the usual
    p1p2_z = (p1_z - p2_z); // !!! Rab = b - a

    for(tt=0; tt <= (ijkA[0]+ijkB[0]); tt++)
        e_t_il[tt] = qcinteg__gg_in_h(ijkA[0],zetaA,xyzA[0], ijkB[0],zetaB,xyzB[0], tt);

    for(uu=0; uu <= (ijkA[1]+ijkB[1]); uu++)
        e_u_jm[uu] = qcinteg__gg_in_h(ijkA[1],zetaA,xyzA[1], ijkB[1],zetaB,xyzB[1], uu);

    for(vv=0; vv <= (ijkA[2]+ijkB[2]); vv++)
        e_v_kn[vv] = qcinteg__gg_in_h(ijkA[2],zetaA,xyzA[2], ijkB[2],zetaB,xyzB[2], vv);

    for(tau=0; tau <= (ijkC[0]+ijkD[0]); tau++)
        e_tau_or[tau] = qcinteg__gg_in_h(ijkC[0],zetaC,xyzC[0], ijkD[0],zetaD,xyzD[0], tau);

    for(nu=0; nu <= (ijkC[1]+ijkD[1]); nu++)
        e_nu_ps[nu] = qcinteg__gg_in_h(ijkC[1],zetaC,xyzC[1], ijkD[1],zetaD,xyzD[1], nu);

    for(phi=0; phi <= (ijkC[2]+ijkD[2]); phi++)
        e_phi_qt[phi] = qcinteg__gg_in_h(ijkC[2],zetaC,xyzC[2], ijkD[2],zetaD,xyzD[2], phi);


    // Summation
    result = 0.0;

    part1 = 2*pow(M_PI, 2.5) / (zetaP1*zetaP2 * sqrt(zetaP1+zetaP2));

    for(tt=0; tt <= (ijkA[0]+ijkB[0]); tt++){
        for(uu=0; uu <= (ijkA[1]+ijkB[1]); uu++){
            for(vv=0; vv <= (ijkA[2]+ijkB[2]); vv++){
                for(tau=0; tau <= (ijkC[0]+ijkD[0]); tau++){
                    for(nu=0; nu <= (ijkC[1]+ijkD[1]); nu++){
                        for(phi=0; phi <= (ijkC[2]+ijkD[2]); phi++){

                            result += ((((tau+nu+phi) % 2)==0) ? 1.0 : -1.0) *\
                              e_t_il[tt]*e_u_jm[uu]*e_v_kn[vv]*e_tau_or[tau]*e_nu_ps[nu]*e_phi_qt[phi] *\
                              qcinteg__hci(0, tt+tau, uu+nu, vv+phi, alpha, p1p2_x,p1p2_y,p1p2_z);

                        }
                    }
                }
            }
        }
    }

    result *= part1;

    // free memory
    free(e_t_il);

    return result;
}


int main()
{
  int ijkA[3] = {0,0,0},
      ijkB[3] = {0,1,0},
      ijkC[3] = {0,0,2},
      ijkD[3] = {2,0,0};

  double xyzA[3] = {0.,0.,0.},
         xyzB[3] = {1.,3.,1.},
         xyzC[3] = {1.,0.,0.},
         xyzD[3] = {1.,0.,4.};

  double zetaA = 2.,
         zetaB = 1.,
         zetaC = 1.,
         zetaD = 2.;


  double result1= qcinteg__eri(ijkA, zetaA, xyzA,
                               ijkB, zetaB, xyzB,
                               ijkC, zetaC, xyzC,
                               ijkD, zetaD, xyzD);

  double result2= qcinteg__eri(ijkD, zetaD, xyzD,
                               ijkC, zetaC, xyzC,
                               ijkB, zetaB, xyzB,
                               ijkA, zetaA, xyzA);


  std::cout << result1 << " " << result2 << std::endl;
  return 0;
}



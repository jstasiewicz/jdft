#pragma once

#include <cmath>

#include "generic.hpp"
#include "contraction.hpp"
#include "hermiteIntegrals.hpp"

/******************************************************************************
 * Calculation of Coulomb integrals, namely:
 *     \int_{\bf R^3} G_1(r) \frac{1}{|r-r_N|} G_2(r) dr
 * where G_1 and G_2 are Gaussian-type orbitals and r_N is the nucleus position.
 * The implementation follows MD scheme.
 *****************************************************************************/
namespace jdft
{
//-----------------------------------------------------------------------------
//////////////////////////////COMPUTATIONAL LAYER//////////////////////////////
//-----------------------------------------------------------------------------
/**
 Nuclear Attraction Integral of two 3Dim cartesian primitive GTOs

 For one electron and a nucleus with charge Z,
 don't forget to multiply by (-Z)
*/

template <int l1, int m1, int n1,
          int l2, int m2, int n2>
fptype nuclearIntegralBase(const GTOBase& g1,
                           const GTOBase& g2,
                           const vec3f& N)
{
  JDFT_RESTRICT fptype E_til[l1 + l2 + 1],
                       E_ujm[m1 + m2 + 1],
                       E_vkn[n1 + n2 + 1];

  fptype bP = g1.b + g2.b;
  const vec3f P  = (g1.b*g1.center + g2.b*g2.center)/bP,
              PN = P - N;

  EvalHermiteExpansionFunctor<l1,l2> ehef_til(g1.b,g1.center.x, g2.b,g2.center.x, E_til);
  EvalHermiteExpansionFunctor<m1,m2> ehef_ujm(g1.b,g1.center.y, g2.b,g2.center.y, E_ujm);
  EvalHermiteExpansionFunctor<n1,n2> ehef_vkn(g1.b,g1.center.z, g2.b,g2.center.z, E_vkn);

  mpl_for<l1+l2>::eval(ehef_til);
  mpl_for<m1+m2>::eval(ehef_ujm);
  mpl_for<n1+n2>::eval(ehef_vkn);

  EvalHCI_NAI_Functor ehcinaif(bP, PN, E_til, E_ujm, E_vkn);
  mpl_for<l1+l2, mpl_for<m1+m2, mpl_for<n1+n2>>>::eval(ehcinaif);

  fptype retVal = ehcinaif.result;
  //fptype retVal = EvalHCI_NAI<l1+l2,m1+m2,n1+n2>::eval(bP, PN, E_til, E_ujm, E_vkn);

  return -g1.coef*g2.coef*retVal*2.*M_PI/bP;
}
//-----------------------------------------------------------------------------
///////////////////////////////////TOPLEVEL////////////////////////////////////
//-----------------------------------------------------------------------------
template <int l1, int m1, int n1,
          int l2, int m2, int n2>
fptype nuclearAttractionIntegral(const GTO<l1, m1, n1>& g1,
                                 const GTO<l2, m2, n2>& g2,
                                 const vec3f& N)
{
  return nuclearIntegralBase<l1,m1,n1,l2,m2,n2>(g1, g2, N);
}
//-----------------------------------------------------------------------------
} // End of namespace jdft

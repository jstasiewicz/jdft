#pragma once

#include "generic.hpp"
#include "system.hpp"
#include "integrals.hpp"

namespace jdft
{
//-----------------------------------------------------------------------------
struct SimpleHamiltonian
{
  void operator()(MatrixXc& F,
                    const System& system)
  {
    size_t numBasisFuns = system.basisFuns.size();

    for(size_t i = 0; i < numBasisFuns; ++i)
      for(size_t j = 0; j <= i; ++j)
      {
        ComplexType T, V_en, I, K;
        T = kineticEnergyIntegral(system.basisFuns[i], system.basisFuns[j]);

        I = K = 0.;
        for(size_t k = 0; k < numBasisFuns; ++k)
        {
          // TODO: mind the coefficients!
          I += electronRepulsionIntegral(system.basisFuns[i], system.basisFuns[j],
                                         system.basisFuns[k], system.basisFuns[k]);
          K += electronRepulsionIntegral(system.basisFuns[i], system.basisFuns[k],
                                         system.basisFuns[k], system.basisFuns[j]);
        }

        V_en = 0.;
        for(auto atom: system.atoms)
        {
          const fptype nuclearCharge   = atom->getNuclearCharge();
          const vec3f& nucleusPosition = atom->getNucleusPosition();
          V_en -= nuclearCharge*nuclearAttractionIntegral(system.basisFuns[i],
                                                          system.basisFuns[j],
                                                          nucleusPosition );
        }
        F(i,j) = T + V_en + ComplexType(2.)*I - K;
        F(j,i) = std::conj(F(i,j));
      }
  }
};
//-----------------------------------------------------------------------------
class SCFSolver
{
  public:

  SCFSolver(System& _system) : system(_system),
                               gsaes(_system.basisFuns.size())
  {
    size_t numBasisFuns = system.basisFuns.size();

    S = MatrixXc(numBasisFuns, numBasisFuns);
    for(size_t i = 0; i < numBasisFuns; ++i)
      for(size_t j = 0; j <= i; ++j)
      {
          S(i,j) = S(j,i) = overlapIntegral(system.basisFuns[i], system.basisFuns[j]);
      }

    C = MatrixXc(numBasisFuns, numBasisFuns);
    for(size_t i = 0; i < numBasisFuns && i < system.numElectrons; ++i)
      C(i,i) = 1.; // TODO: add a better guess.

    F = MatrixXc(numBasisFuns, numBasisFuns);
  }

  void doIteration()
  {
    gsaes.compute(F, S, Eigen::Ax_lBx|Eigen::ComputeEigenvectors);
    std::cout << gsaes.eigenvectors() << std::endl; // DEBUG
  }

  MatrixXc S, C, F; // Self consistent field, MOFOs!

  private:
    System& system;
    Eigen::GeneralizedSelfAdjointEigenSolver<MatrixXc> gsaes;
};
//-----------------------------------------------------------------------------
} // End of namespace jdft

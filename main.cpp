#include <iostream>
#include <iomanip>
#include <vector>
#include <numeric>

#include "jdft"
#include "basis/6311pGx.hpp"

#include "tests.hpp"

using namespace std;
using namespace jdft;

void doOrca();

int main()
{
  cout << "dupa" << endl;
  //doOrca();
  //testAll();

  
  vec3f pos1(0.,0.,0.);
  vec3f pos2(1.,3.,1.);
  vec3f pos3(1.,0.,0.);
  vec3f pos4(1.,0.,4.);
  std::shared_ptr<G000> g0 = std::make_shared<G000>(1.,2.,pos1);
  std::shared_ptr<G000> g1 = std::make_shared<G000>(3.,2.,pos1);
  std::shared_ptr<G010> g2 = std::make_shared<G010>(1.,1.,pos2);
  std::shared_ptr<G010> g3 = std::make_shared<G010>(3.,2.,pos2);
  std::shared_ptr<G002> g4 = std::make_shared<G002>(2.,1.,pos3);
  std::shared_ptr<G002> g5 = std::make_shared<G002>(3.,1.,pos3);
  std::shared_ptr<G200> g6 = std::make_shared<G200>(3.,2.,pos4);
  std::shared_ptr<G200> g7 = std::make_shared<G200>(3.,1.,pos4);

  auto cont1 = Contraction({g0,g1});
  auto cont2 = Contraction({g2,g3});
  auto cont3 = Contraction({g4,g5});
  auto cont4 = Contraction({g6,g7});

    cout << electronRepulsionIntegral(g0,g2,g4,g6) << endl;
//  cout << electronRepulsionIntegral(g0,g2,g6,g4) << endl;
//  cout << electronRepulsionIntegral(g2,g0,g4,g6) << endl;
//  cout << electronRepulsionIntegral(g2,g0,g6,g4) << endl;
//  cout << electronRepulsionIntegral(g4,g6,g0,g2) << endl;
//  cout << electronRepulsionIntegral(g4,g6,g2,g0) << endl;
//  cout << electronRepulsionIntegral(g6,g4,g0,g2) << endl;
  cout << electronRepulsionIntegral(g6,g4,g2,g0) << endl << endl;

  cout << setprecision(16);
  cout << electronRepulsionIntegral(cont4,cont3,cont2,cont1) << endl;
  cout << electronRepulsionIntegral(cont1,cont2,cont3,cont4) << endl;

/*
  cout <<  electronRepulsionIntegral(cont1, cont2, cont3, cont4) << endl;
  cout <<  electronRepulsionIntegral(cont1, cont2, cont4, cont3) << endl;
  cout <<  electronRepulsionIntegral(cont2, cont1, cont3, cont4) << endl;
  cout <<  electronRepulsionIntegral(cont2, cont1, cont4, cont3) << endl;
  cout <<  electronRepulsionIntegral(cont3, cont4, cont1, cont2) << endl;
  cout <<  electronRepulsionIntegral(cont3, cont4, cont2, cont1) << endl;
  cout <<  electronRepulsionIntegral(cont4, cont3, cont1, cont2) << endl;
  cout <<  electronRepulsionIntegral(cont4, cont3, cont2, cont1) << endl;
*/
  return 0;
}
//-----------------------------------------------------------------------------
void doOrca()
{
  vec3f pos1(0.,0.,0.);
  _6311pGx::C wegiel(pos1);
  _6311pGx::N azot(pos1);
  _6311pGx::O tlen(pos1);
  _6311pGx::P fosfor(pos1);

  for(auto cBasisFun : wegiel.getBasisFuns())
    for(auto nBasisFun : azot.getBasisFuns())
    {
//        cout << overlapIntegral(cBasisFun, nBasisFun);
    }

}



#pragma once

#include <tuple>

#include "generic.hpp"
#include "utils.hpp"
#include "contraction.hpp"
#include "mpl.hpp"

#include "overlapIntegral.hpp"
#include "kineticIntegral.hpp"
#include "nuclearIntegral.hpp"
#include "electronRepulsionIntegral.hpp"

namespace jdft
{
//-----------------------------------------------------------------------------
#pragma inline_recursion(on)
//-----------------------------------------------------------------------------
/*  NOTE: detection of integrals like (i|i) or (ij|ij), where certain
    indices are repeated is done, but not exploited for performance. */
//-----------------------------------------------------------------------------
/** Entry point to calculate overlap integrals of contracted gaussians. */
fptype overlapIntegral(const Contraction& contraction1,
                       const Contraction& contraction2)
{
  fptype retVal = 0.;

  return retVal;
}

#undef JDFT_OIA_MACRO
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
fptype kineticEnergyIntegral(const Contraction& contraction1,
                             const Contraction& contraction2)
{
  fptype retVal = 0.;

  return retVal;
}

#undef JDFT_KIA_MACRO
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
fptype nuclearAttractionIntegral(const Contraction& contraction1,
                                 const Contraction& contraction2,
                                 const vec3f& nucleusPos)
{
  fptype retVal = 0.;

  return retVal;
}

#undef JDFT_NAIA_MACRO
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
fptype electronRepulsionIntegralImpl(const Contraction& contraction1,
                                     const Contraction& contraction2,
                                     const Contraction& contraction3,
                                     const Contraction& contraction4)
{
  fptype retVal = 0.;

  return retVal;
}
//-----------------------------------------------------------------------------
#pragma inline_recursion(off)
//-----------------------------------------------------------------------------

} // End of namespace jdft

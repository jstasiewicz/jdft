/* qcinteg__nai.c
 *
 * Nuclear Attraction Integral
 */

/*
    Copyright 2008, 2009, Jan Šmydke
     All rights reserved.

    This file is a part of "qcinteg"

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

      1. Redistributions of source code must retain the above copyright
         notice, this list of conditions and the following disclaimer.
      2. Redistributions in binary form must reproduce the above copyright
         notice, this list of conditions and the following disclaimer in the
         documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstdlib> // calloc
#include <cmath>
#include "orbitals.hpp"
#include "generic.hpp"

#define hci000(n,alpha,x,y,z) (pow(-2*alpha,(n)) * boysFunction((n), alpha * (x*x + y*y + z*z)))

#define key_000 0
#define key_100 256
#define key_010 16
#define key_001 1
#define key_200 512
#define key_020 32
#define key_002 2
#define key_110 272
#define key_101 257
#define key_011 17
#define key_300 768
#define key_030 48
#define key_003 3
#define key_210 528
#define key_201 513
#define key_120 288
#define key_021 33
#define key_102 258
#define key_012 18
#define key_111 273

//-----------------------------------------------------------------------------
template<int i, int j, int t>
fptype qcinteg__gg_in_h(fptype b1, fptype x1, fptype b2, fptype x2)
{
    const fptype gamma_C = (b1 + b2),
                       C = (b1*x1 + b2*x2)/gamma_C,
                      ab = x2 - x1;
    fptype E = 0.;

    if(t < 0 || t > (i + j))
    {
        E = 0.0;
    }
    else
    {
        if(t==0 && i==0 && j==0)
        {
            E = exp(- b1 * b2 * ab*ab/(b1 + b2));
        }
  else
  {
    if(i > 0) // i>0, j>=0, t>=0
    {
      if(t==0)
      {
        if(i==1 && j==0)
        {
          E = exp(- b1*b2*ab*ab/(b1 + b2))*(C-x1);
        }
        else
        {
          E = qcinteg__gg_in_h<i-1,j,1>(b1,x1, b2,x2) + qcinteg__gg_in_h<i-1,j,0>(b1,x1, b2,x2)*(C-x1);
        }
    }
    else // i>0, j>=0, t>0
    {
        if((t+2) > (i+j))
        {
      if((t+1) > (i+j))
      {
          if(t > (i+j))
          {
        E = 0.0;
          }
          else
          {
            E = qcinteg__gg_in_h<i-1,j,t-1>(b1,x1, b2,x2)/(2*gamma_C);
          }
      }
      else
      {
         E = qcinteg__gg_in_h<i-1,j,t>(b1,x1, b2,x2) * (C - x1) +
         qcinteg__gg_in_h<i-1,j,t-1>(b1,x1, b2,x2) / (2*gamma_C);
      }
        }
        else
        {
          E = qcinteg__gg_in_h<i-1,j,t+1>(b1,x1, b2,x2)*(t+1) +
              qcinteg__gg_in_h<i-1,j,t>(  b1,x1, b2,x2)*(C-x1) +
              qcinteg__gg_in_h<i-1,j,t-1>(b1,x1, b2,x2)/(2*gamma_C);
        }
    }
            }
      else // i==0, j>0
      {
        if(t == 0)
        {
          if(j==1)
          {
            E = exp(-b1*b2*ab*ab/(b1+b2))*(C-x2);
          }
        else // i==0, j>1
        {
          E = qcinteg__gg_in_h<0,j-1,1>(b1,x1, b2,x2) + qcinteg__gg_in_h<0,j-1,0>(b1,x1, b2,x2) * (C - x2);
        }
        }
    else // i==0, j>0, t>0
    {
        if(t > j)
        {
          E = 0.0;
        }
        else // i==0, j>0, 0 < t <= j
        {
          if(t==j)
          {
            E = qcinteg__gg_in_h<0,j-1,t-1>(b1,x1, b2,x2) / (2*gamma_C);
          }
          else // i==0, j>0, 0 < t < j
          {
            E = qcinteg__gg_in_h<0,j-1,t+1>(b1,x1, b2,x2) * (t+1) +\
              qcinteg__gg_in_h<0,j-1,t>(b1,x1, b2,x2) * (C - x2) +\
              qcinteg__gg_in_h<0,j-1,t-1>(b1,x1, b2,x2) / (2*gamma_C);
          }
        }
    }
  }
        }
    }

    return E;
}
//-----------------------------------------------------------------------------
template <int lmn1, int lmn2, int tuv, int maxIter> 
struct EvalE
{
  static void eval(fptype b1, fptype x1, fptype b2, fptype x2, JDFT_RESTRICT fptype* E)
  {
    E[tuv] = qcinteg__gg_in_h<lmn1,lmn2,tuv>(b1, x1, b2, x2)
             + EvalE<lmn1, lmn2, tuv+1, maxIter>::eval(b1, x1, b2, x2);
  }
};
template <int lmn1, int lmn2, int maxIter> 
struct EvalE<lmn1, lmn2, maxIter, maxIter> 
{
  static void eval(fptype b1, fptype x1, fptype b2, fptype x2, JDFT_RESTRICT fptype* E)
  {
    E[maxIter] = qcinteg__gg_in_h<lmn1,lmn2,maxIter>(b1, x1, b2, x2);
  }
};
//-----------------------------------------------------------------------------
/**
 * Returns Hermite Coulomb Integral
 *
 * ie Rtuv^n(alpha, Rpc) in Helgaker
 *
 * n==0 for true HCI
 */
fptype qcinteg__hci(int n, int t,int u,int v, fptype alpha, fptype pc_x, fptype pc_y, fptype pc_z)
{
    fptype result;

    unsigned int key = 0;


    /* set key */
    if(t>14)
    {
  key |= 15; // ie at most 14 will be treated individually
    }
    else
    {
  key |= (unsigned int) t;
    }
    key <<= 4;
    if(u>14)
    {
  key |= 15;
    }
    else
    {
  key |= (unsigned int) u;
    }
    key <<= 4;
    if(v>14)
    {
  key |= 15;
    }
    else
    {
  key |= (unsigned int) v;
    }
    /* key now set to "tuv" */

    /* "Pattern matching" */
    switch (key)
    {
  case key_000:
      result = hci000(n, alpha, pc_x, pc_y, pc_z);
      break;
  case key_100:
      result = pc_x * hci000(n+1, alpha, pc_x, pc_y, pc_z);
      break;
  case key_010:
      result = pc_y * hci000(n+1, alpha, pc_x, pc_y, pc_z);
      break;
  case key_001:
      result = pc_z * hci000(n+1, alpha, pc_x, pc_y, pc_z);
      break;
  case key_200:
      result = hci000(n+1, alpha, pc_x, pc_y, pc_z) + pc_x*pc_x*hci000(n+2, alpha, pc_x, pc_y, pc_z);
      break;
  case key_020:
      result = hci000(n+1, alpha, pc_x, pc_y, pc_z) + pc_y*pc_y*hci000(n+2, alpha, pc_x, pc_y, pc_z);
      break;
  case key_002:
      result = hci000(n+1, alpha, pc_x, pc_y, pc_z) + pc_z*pc_z*hci000(n+2, alpha, pc_x, pc_y, pc_z);
      break;
  case key_110:
      result = pc_x * pc_y * hci000(n+2, alpha, pc_x, pc_y, pc_z);
      break;
  case key_101:
      result = pc_x * pc_z * hci000(n+2, alpha, pc_x, pc_y, pc_z);
      break;
  case key_011:
      result = pc_y * pc_z * hci000(n+2, alpha, pc_x, pc_y, pc_z);
      break;
  case key_300:
      result = 3*pc_x*hci000(n+2, alpha, pc_x, pc_y, pc_z) + pc_x*pc_x*pc_x*hci000(n+3, alpha, pc_x, pc_y, pc_z);
      break;
  case key_030:
      result = 3*pc_y*hci000(n+2, alpha, pc_x, pc_y, pc_z) + pc_y*pc_y*pc_y*hci000(n+3, alpha, pc_x, pc_y, pc_z);
      break;
  case key_003:
      result = 3*pc_z*hci000(n+2, alpha, pc_x, pc_y, pc_z) + pc_z*pc_z*pc_z*hci000(n+3, alpha, pc_x, pc_y, pc_z);
      break;
  case key_210:
      result = pc_y*hci000(n+2, alpha, pc_x, pc_y, pc_z) + pc_x*pc_y*hci000(n+3, alpha, pc_x, pc_y, pc_z);
      break;
  case key_201:
      result = pc_z*hci000(n+2, alpha, pc_x, pc_y, pc_z) + pc_x*pc_z*hci000(n+3, alpha, pc_x, pc_y, pc_z);
      break;
  case key_120:
      result = pc_x*hci000(n+2, alpha, pc_x, pc_y, pc_z) + pc_x*pc_y*hci000(n+3, alpha, pc_x, pc_y, pc_z);
      break;
  case key_021:
      result = pc_z*hci000(n+2, alpha, pc_x, pc_y, pc_z) + pc_y*pc_z*hci000(n+3, alpha, pc_x, pc_y, pc_z);
      break;
  case key_102:
      result = pc_x*hci000(n+2, alpha, pc_x, pc_y, pc_z) + pc_x*pc_z*hci000(n+3, alpha, pc_x, pc_y, pc_z);
      break;
  case key_012:
      result = pc_y*hci000(n+2, alpha, pc_x, pc_y, pc_z) + pc_y*pc_z*hci000(n+3, alpha, pc_x, pc_y, pc_z);
      break;
  case key_111:
      result = pc_x * pc_y * pc_z * hci000(n+3, alpha, pc_x, pc_y, pc_z);
  default:
      if(t > 0)
      {
    if(t==1)
    {
        result = pc_x * qcinteg__hci(n+1, 0,u,v, alpha, pc_x, pc_y, pc_z);
    }
    else // t>1
    {
        result = (t-1)*qcinteg__hci(n+1, t-2,u,v, alpha, pc_x, pc_y, pc_z) +\
      pc_x*qcinteg__hci(n+1, t-1,u,v, alpha, pc_x, pc_y, pc_z);
    }
      }
      else // t==0
      {
    if(u > 0)
    {
        if(u==1)
        {
      result = pc_y * qcinteg__hci(n+1, 0,0,v, alpha, pc_x, pc_y, pc_z);
        }
        else
        {
      result = (u-1)*qcinteg__hci(n+1, 0,u-2,v, alpha, pc_x, pc_y, pc_z) +\
          pc_y*qcinteg__hci(n+1, 0,u-1,v, alpha, pc_x, pc_y, pc_z);
        }
    }
    else // t==0, u==0, v>3
    {
        result = (v-1)*qcinteg__hci(n+1, 0,0,v-2, alpha, pc_x, pc_y, pc_z) +\
      pc_z*qcinteg__hci(n+1, 0,0,v-1, alpha, pc_x, pc_y, pc_z);
    }
      }
      break;
    }

    return result;
}

//-----------------------------------------------------------------------------
/**
 Nuclear Attraction Integral of two 3Dim cartesian primitive GTOs

 GTOs Ced on A, B
 Nucleus Ced on N

 For one electron and a nucleus with charge Z,
 don't forget to multiply by (-Z)
 */

template<int l1, int m1, int n1,
         int l2, int m2, int n2>
fptype nuclearIntegralBase(const GTOBase& g1, const GTOBase& g2, const vec3f& N)
{
    fptype retVal   = 0., 
           my_gamma = g1.b + g2.b;

    JDFT_RESTRICT fptype E_til[l1 + l2 + 1],
                         E_ujm[m1 + m2 + 1],
                         E_vkn[n1 + n2 + 1];

    const vec3f P  = (g1.b*g1.center + g2.b*g2.center)/my_gamma,
                PN = P - N;  
/*
    for(t=0; t <= (l1+l2); t++)
        E_til[t] = qcinteg__gg_in_h<l1,l2,t>(g1.b,g1.center.x, g2.b,g2.center.x);
    for(u=0; u <= (ijkA[1]+ijkB[1]); u++)
        E_ujm[u] = qcinteg__gg_in_h<m1,m2,u>(g1.b,g1.center.x, g2.b,g2.center.x);
    for(v=0; v <= (ijkA[2]+ijkB[2]); v++)
        E_vkn[v] = qcinteg__gg_in_h<n1,n2,v>(g1.b,g1.center.x, g2.b,g2.center.x);

    EvalE<l1,l2,0,l1+l2>::eval(g1.b,g1.center.x, g2.b,g2.center.x, E_til);
    EvalE<m1,m2,0,m1+m2>::eval(g1.b,g1.center.y, g2.b,g2.center.y, E_ujm);
    EvalE<n1,n2,0,n1+n2>::eval(g1.b,g1.center.z, g2.b,g2.center.z, E_vkn);
*/
    //EvalE<l1,l2,0,0>::eval(g1.b,g1.center.x, g2.b,g2.center.x, E_til);
    E_til[0] = qcinteg__gg_in_h<1,1,0>(g1.b,g1.center.x, g2.b,g2.center.x);
    
    for(int t=0; t <= l1+l2; ++t)
        for(int u=0; u <= m1+m2; ++u)
            for(int v=0; v <= n1+n2; ++v)
                retVal += E_til[t]*E_ujm[u]*E_vkn[v] * qcinteg__hci(0, t,u,v, my_gamma, PN.x,PN.y,PN.z);

    retVal *= 2.* M_PI/my_gamma;

    return retVal;
}
//-----------------------------------------------------------------------------
template<int l1, int m1, int n1,
         int l2, int m2, int n2>
inline fptype nuclearIntegral(const GTOTemp<l1, m1, n1>& A, const GTOTemp<l2, m2, n2>& B, const vec3f& N)
{
  return nuclearIntegralBase<l1,m1,n1,l2,m2,n2>(A, B, N);
}

//-----------------------------------------------------------------------------
